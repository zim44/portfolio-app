@extends('auth.layouts.master')
@section('title', __('Verify Email'))
@section('content')
    <div class="mh-home image-bg home-2-img" id="mh-home" style="height: 100%">
        <div class="img-foverlay img-color-overlay" style="height: 100%">
            <div class="container">
                <div class="row section-separator xs-column-reverse vertical-middle-content" style="padding-top: 17%">
                    <div class="col-sm-6">
                        <h1 class="mb-3 text-18">{{__('Verify Email')}}</h1>
                        {{ Form::open(['route' => 'verifyEmailProcess', 'method' => 'post']) }}
                            <div class="form-group">
                                <label for="verification_code">{{__('Verification Code')}}</label>
                                <input name="verification_code" id="verification_code" class="form-control form-control-rounded" type="text" value="{{ old('verification_code') }}">
                                @if (isset($errors) && $errors->has('verification_code'))
                                    <span class="text-danger"><strong>{{ $errors->first('verification_code') }}</strong></span>
                                @endif
                            </div>
                            <button class="btn btn-rounded btn-primary btn-block mt-2">{{__('Submit')}}</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
