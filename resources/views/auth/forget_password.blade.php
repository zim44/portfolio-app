@extends('auth.layouts.master')
@section('title', __('Reset Password'))
@section('content')
    <div class="mh-home image-bg home-2-img" id="mh-home" style="height: 100%">
        <div class="img-foverlay img-color-overlay" style="height: 100%">
            <div class="container">
                <div class="row section-separator xs-column-reverse vertical-middle-content" style="padding-top: 17%">
                    <div class="col-sm-6">
                        <h1 class="mb-3 text-18">{{__('Reset Password')}}</h1>
                        {{ Form::open(['route' => 'forgetPasswordCodeProcess', 'method' => 'post']) }}
                            <div class="form-group">
                                <label for="reset_password_code">{{__('Reset Password Code')}}</label>
                                <input name="reset_password_code" id="reset_password_code" class="form-control form-control-rounded" type="text" value="{{ old('reset_password_code') }}">
                                @if (isset($errors) && $errors->has('reset_password_code'))
                                    <span class="text-danger"><strong>{{ $errors->first('reset_password_code') }}</strong></span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="password">{{__('New Password')}}</label>
                                <input name="new_password" id="password" class="form-control form-control-rounded" type="password">
                                @if (isset($errors) && $errors->has('new_password'))
                                    <span class="text-danger"><strong>{{ $errors->first('new_password') }}</strong></span>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation">{{__('Confirm Password')}}</label>
                                <input name="password_confirmation" id="password_confirmation" class="form-control form-control-rounded" type="password">
                                @if (isset($errors) && $errors->has('password_confirmation'))
                                    <span class="text-danger"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                                @endif
                            </div>
                            <button class="btn btn-rounded btn-primary btn-block mt-2">{{__('Submit')}}</button>
                        {{ Form::close() }}
                        <div class="mt-3 text-center">
                            <a href="{{ route('signIn') }}" class="text-muted"><u>{{__('Sign In')}}</u></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
