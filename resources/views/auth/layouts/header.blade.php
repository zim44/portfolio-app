<!--
===================
   NAVIGATION
===================
-->
<header class="black-bg mh-header mh-fixed-nav nav-scroll mh-xs-mobile-nav" id="mh-header">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-lg mh-nav nav-btn">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon icon"></span>
                </button>

                <div class="logo display-4" id="">Portfolio</div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        @if(isset($menu)&&$menu=='get-started')
                            <li class="nav-item  active">
                                <a class="nav-link" href="{{ route('signIn') }}">Sign In</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('signUp') }}">Sign Up</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
