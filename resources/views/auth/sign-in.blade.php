@extends('auth.layouts.master')
@section('title', __('Sign In'))
@section('content')
    <div class="mh-home image-bg home-2-img" id="mh-home" style="height: 100%">
        <div class="img-foverlay img-color-overlay" style="height: 100%">
            <div class="container">
                <div class="row section-separator xs-column-reverse vertical-middle-content" style="padding-top: 15%;">
                    <div class="col-sm-6">
                        <div class="mh-header-info">
                            <h1 class="mb-3 text-18">{{__('Sign In')}}</h1>
                            {{ Form::open(['route' => 'signInProcess', 'method' => 'post']) }}
                                <div class="form-group">
                                    <label for="email_username">{{__('Email/Username')}}</label>
                                    <input name="email_username" id="email_username" class="form-control form-control-rounded" type="text" value="{{old('email_username')}}">
                                    @if (isset($errors) && $errors->has('email_username'))
                                        <span class="text-danger"><strong>{{ $errors->first('email_username') }}</strong></span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password">{{__('Password')}}</label>
                                    <input name="password" id="password" class="form-control form-control-rounded" type="password">
                                    @if (isset($errors) && $errors->has('password'))
                                        <span class="text-danger"><strong>{{ $errors->first('password') }}</strong></span>
                                    @endif
                                </div>
                                <button class="btn btn-rounded btn-primary btn-block mt-2">{{__('Sign In')}}</button>
                            {{ Form::close() }}
                            <div class="mt-3 text-center">
                                <a href="{{ route('forgetPassword') }}" class="text-muted"><u>{{__('Forgot Password')}}?</u></a>
                                 | Doesn't have account yet? <a href="{{ route('signUp') }}" class="text-muted"><u>{{__('Create One')}}</u></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
