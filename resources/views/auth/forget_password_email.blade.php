@extends('auth.layouts.master')
@section('title', __('Forget Password'))
@section('content')
    <div class="mh-home image-bg home-2-img" id="mh-home" style="height: 100%">
        <div class="img-foverlay img-color-overlay" style="height: 100%">
            <div class="container">
                <div class="row section-separator xs-column-reverse vertical-middle-content" style="padding-top: 25%">
                    <div class="col-sm-6">
                        <h1 class="mb-3 text-18">{{__('Reset Password')}}</h1>
                        {{ Form::open(['route' => 'forgetPasswordEmailSendProcess', 'method' => 'post']) }}
                            <div class="form-group">
                                <label for="email_username">{{__('Email/Username')}}</label>
                                <input name="email_username" id="email_username" class="form-control form-control-rounded" type="text" value="{{ old('email_username') }}">
                                @if (isset($errors) && $errors->has('email_username'))
                                    <span class="text-danger"><strong>{{ $errors->first('email_username') }}</strong></span>
                                @endif
                            </div>
                            <button class="btn btn-rounded btn-primary btn-block mt-2">{{__('Submit')}}</button>
                        {{ Form::close() }}
                        <div class="mt-3 text-center">
                            <a href="{{ route('signIn') }}" class="text-muted"><u>{{__('Sign In')}}</u></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
