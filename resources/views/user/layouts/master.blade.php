<!DOCTYPE html>
<html lang="en" dir="" @if (userSetting('theme')==THEME_LIGHT) style="background-color: #fafafa;"@endif>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="Personal cv/resume website for professional and personal use." />
    <meta name="keywords" content="creative, cv, designer,  online cv, online resume, powerful portfolio, professional, professional resume, responsive, resume, vcard " />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title')</title>
    <!-- FAV AND ICONS   -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.ico')}}">
    <link rel="shortcut icon" href="{{asset('assets/images/apple-icon.png')}}">
    <link rel="shortcut icon" sizes="72x72" href="{{asset('assets/images/apple-icon-72x72.png')}}">
    <link rel="shortcut icon" sizes="114x114" href="{{asset('assets/images/apple-icon-114x114.png')}}">

    <!-- Google Font-->
    <link href="http://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('assets/icons/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('assets/plugins/css/bootstrap.min.css')}}">
    <!-- Animate CSS-->
    <link rel="stylesheet" href="{{asset('assets/plugins/css/animate.css')}}">
    <!-- Owl Carousel CSS-->
    <link rel="stylesheet" href="{{asset('assets/plugins/css/owl.css')}}">
    <!-- Fancybox-->
    <link rel="stylesheet" href="{{asset('assets/plugins/css/jquery.fancybox.min.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/dropify.min.css')}}">

    <!-- Custom CSS-->
    <link rel="stylesheet" href="{{asset('assets/css/styles.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/developed.css')}}">

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.jqueryui.min.css">

    <!-- Colors -->
    @if(userSetting('fill') == FILL_PURPLE) <link rel="stylesheet" href="{{asset('assets/css/colors/purple.css')}}" title="purple">
    @elseif(userSetting('fill') == FILL_RED) <link rel="stylesheet" href="{{asset('assets/css/colors/red.css')}}" title="red">
    @elseif(userSetting('fill') == FILL_BLUE) <link rel="stylesheet" href="{{asset('assets/css/colors/blue.css')}}" title="blue">
    @elseif(userSetting('fill') == FILL_YELLOW) <link rel="stylesheet" href="{{asset('assets/css/colors/yellow.css')}}" title="yellow">
    @elseif(userSetting('fill') == FILL_PINK) <link rel="stylesheet" href="{{asset('assets/css/colors/pink.css')}}" title="pink">
    @elseif(userSetting('fill') == FILL_GREEN) <link rel="stylesheet" href="{{asset('assets/css/colors/green.css')}}" title="green">
    @elseif(userSetting('fill') == FILL_DARK_GREEN) <link rel="stylesheet" href="{{asset('assets/css/colors/dark-green.css')}}" title="dark-green">
    @else <link rel="stylesheet" href="{{asset('assets/css/colors/defauld.css')}}" title="defauld">
    @endif
    @yield('style')
</head>

<body class="@if (userSetting('theme')==THEME_LIGHT) white-vertion @else dark-vertion @endif dark-bg">
<div class="app-admin-wrap">

    <!-- Start Loader -->
    <div class="section-loader">
        <div class="loader">
            <div></div>
            <div></div>
        </div>
    </div>
    <!-- End Loader -->
    @if (!empty(Auth::user()) && Auth::user()->role == USER_ROLE)
        @if(isset($mainMenu) && $mainMenu=='preview')
            @include('user.layouts.preview-header')
        @elseif(isset($mainMenu) && $mainMenu=='customization')
            @include('user.layouts.customization-header')
        @elseif(isset($mainMenu) && $mainMenu=='account')
            @include('user.layouts.account-header')
        @endif
    @endif
    <div class="container float-message">
        <div class="alert-float alert align-top" id="alert" style="display: none">
            <button type="button" class="close" id="alertRemove" aria-hidden="true">x</button>
            <span class="message"></span>
        </div>
        @if(Session::has('success'))
            <div class="alert-float alert alert-success alert-dismissable text-dark" style="margin-top: 100px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                {{Session::get('success')}}
            </div>
        @endif
        @if(Session::has('error'))
            <div class="alert-float alert alert-danger alert-dismissable text-dark" style="margin-top: 100px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                {{Session::get('error')}}
            </div>
        @endif
        @if(!empty($errors) && count($errors) > 0)
            <div class="alert-float alert alert-danger alert-dismissable text-dark" style="margin-top: 100px;">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">x</button>
                {{$errors->first()}}
            </div>
        @endif
    </div>
    @yield('content')
</div>

<!-- jQuery first, then Popper.js, then Bootstrap JS -->

<!-- jQuery -->
<script src="{{asset('assets/plugins/js/jquery.min.js')}}"></script>
<!-- popper -->
<script src="{{asset('assets/plugins/js/popper.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('assets/plugins/js/bootstrap.min.js')}}"></script>
<!-- owl carousel -->
<script src="{{asset('assets/plugins/js/owl.carousel.js')}}"></script>
<!-- validator -->
<script src="{{asset('assets/plugins/js/validator.min.js')}}"></script>
<!-- wow -->
<script src="{{asset('assets/plugins/js/wow.min.js')}}"></script>
<!-- mixin js-->
<script src="{{asset('assets/plugins/js/jquery.mixitup.min.js')}}"></script>
<!-- circle progress-->
<script src="{{asset('assets/plugins/js/circle-progress.js')}}"></script>
<!-- jquery nav -->
<script src="{{asset('assets/plugins/js/jquery.nav.js')}}"></script>
<!-- Fancybox js-->
<script src="{{asset('assets/plugins/js/jquery.fancybox.min.js')}}"></script>
<!-- Map api -->
<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&amp;key=AIzaSyCRP2E3BhaVKYs7BvNytBNumU0MBmjhhxc"></script>
<!-- isotope js-->
<script src="{{asset('assets/plugins/js/isotope.pkgd.js')}}"></script>
<script src="{{asset('assets/plugins/js/packery-mode.pkgd.js')}}"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap.min.js"></script>
<!-- Custom Scripts-->
<script src="{{asset('assets/js/map-init.js')}}"></script>
<script src="{{asset('assets/js/custom-scripts.js')}}"></script>
@yield('script')
<script>
    $(document).on("click", ".confirmedDelete", function() {
        var link = $(this).data("link");
        jQuery.confirm({
            content: '{{ __('Do you really want to delete?') }}',
            title: '{{ __('Confirmation') }}',
            buttons: {
                confirm: {
                    text: '{{ __('Yes') }}',
                    keys: ['enter'],
                    action: function(){
                        window.location.replace(link);
                    }
                },
                cancel: {
                    text: '{{ __('No') }}',
                    keys: ['esc'],
                    action: function(){
                    }
                }
            }
        });
    });
    $('#alertRemove').click(function () {
        $('#alert').hide('slow');
    });
    $('.close-content').click(function () {
        $(this).parent().hide('slow');
    });

    $('document').ready(function () {
        setTimeout(function () {
            $('.alert').hide('slow');
        },2000);
    });
</script>
@if (userSetting('theme')==THEME_LIGHT)
    <script>
        $('section').addClass('bg-white');
        $('footer').addClass('bg-white');
    </script>
@endif
@if (!empty(Auth::user()) && Auth::user()->role == USER_ROLE)
    @include('user.layouts.style-switcher')
@endif
</body>
</html>
