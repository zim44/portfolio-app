<!--
===================
   NAVIGATION
===================
-->
<header class="black-bg mh-header mh-fixed-nav nav-scroll mh-xs-mobile-nav" id="mh-header" style="z-index: 99">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-lg mh-nav nav-btn">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon icon"></span>
                </button>
                <div class="logo display-4" id="">Portfolio</div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto ml-auto">
                        <li class="nav-item @if(isset($submenu)&&$submenu=='messages') active disabled @endif">
                            <a class="nav-link" href="{{route('account.messages')}}">Messages</a>
                        </li>
                    </ul>
                </div>
                <div style="margin: auto"></div>

                <div class="header-part-left">
                    <!-- User avatar dropdown -->
                    <div class="dropdown">
                        <div class="align-self-end">
                            <img src="{{profilePicture()}}" height="50" width="50" class="rounded-circle dropdown-button" id="userDropdown" alt="Name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                            <div class="dropdown-menu dropdown-menu-left dark-bg" aria-labelledby="userDropdown" style="left: -80px">
                                <span class="dropdown-item disabled" href="">{{$user->first_name . ' ' . $user->last_name}}</span>
                                <span class="dropdown-item disabled" href="">{{('@') . $user->username}}</span>
                                <hr>
                                <a class="dropdown-item" href="{{route('profile')}}">{{__('Profile')}}</a>
                                <a class="dropdown-item" href="{{route('signOut')}}">{{__('Sign out')}}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>
