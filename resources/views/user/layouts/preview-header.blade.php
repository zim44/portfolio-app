<!--
===================
   NAVIGATION
===================
-->
<header class="black-bg mh-header mh-fixed-nav nav-scroll mh-xs-mobile-nav" id="mh-header">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-lg mh-nav nav-btn">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon icon"></span>
                </button>
                <img src="{{profilePicture()}}" height="50" width="50" class="rounded-circle" alt="Name">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto ml-auto">
                        @foreach(headerMenus() as $headerMenu)
                            @if($headerMenu->menu=='Home')
                                <li class="nav-item"><a class="nav-link" href="#mh-home">Home</a></li>
                            @endif
                            @if($headerMenu->menu=='About')
                                <li class="nav-item"><a class="nav-link" href="#mh-about">About</a></li>
                            @endif
                            @if($headerMenu->menu=='Skills')
                                <li class="nav-item"><a class="nav-link" href="#mh-skills">Skills</a></li>
                            @endif
                            @if($headerMenu->menu=='Experiences')
                                <li class="nav-item"><a class="nav-link" href="#mh-experience">Experiences</a></li>
                            @endif
                            @if($headerMenu->menu=='Portfolio')
                                <li class="nav-item"><a class="nav-link" href="#mh-portfolio">Portfolio</a></li>
                            @endif
                            @if($headerMenu->menu=='Pricing')
                                <li class="nav-item"><a class="nav-link" href="#mh-pricing">Pricing</a></li>
                            @endif
                            @if($headerMenu->menu=='Blog')
                                <li class="nav-item"><a class="nav-link" href="#mh-blog">Blog</a></li>
                            @endif
                            @if($headerMenu->menu=='Contact')
                                <li class="nav-item"><a class="nav-link" href="#mh-contact">Contact</a></li>
                            @endif
                        @endforeach
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
