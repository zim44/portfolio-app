<!--
===================
   NAVIGATION
===================
-->
<header class="black-bg mh-header mh-fixed-nav nav-scroll mh-xs-mobile-nav" id="mh-header" style="z-index: 99">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <nav class="navbar navbar-expand-lg mh-nav nav-btn">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon icon"></span>
                </button>
                <img src="{{profilePicture()}}" height="50" width="50" class="rounded-circle" alt="Name">
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto ml-auto">
                        @foreach(headerMenus() as $headerMenu)
                            @if($headerMenu->menu=='Home')
                                <li class="nav-item @if(isset($submenu)&&$submenu=='home') active disabled @endif">
                                    <a class="nav-link" href="{{route('headerMenu.home')}}">Home</a>
                                </li>
                                @if(isset($submenu)&&$submenu=='home')
                                    <a href="{{route('deleteMenu', encrypt($headerMenu->id))}}" title="Delete '{{$headerMenu->menu}}' Menu" class=" text-muted"
                                    data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                @endif
                            @endif
                            @if($headerMenu->menu=='About')
                                <li class="nav-item @if(isset($submenu)&&$submenu=='about') active disabled @endif">
                                    <a class="nav-link" href="{{route('headerMenu.about')}}">About</a>
                                </li>
                                @if(isset($submenu)&&$submenu=='about')
                                    <a href="{{route('deleteMenu', encrypt($headerMenu->id))}}" title="Delete '{{$headerMenu->menu}}' Menu" class=" text-muted"
                                    data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                @endif
                            @endif
                            @if($headerMenu->menu=='Skills')
                                <li class="nav-item @if(isset($submenu)&&$submenu=='skills') active disabled @endif">
                                    <a class="nav-link" href="{{route('headerMenu.skills')}}">Skills</a>
                                </li>
                                @if(isset($submenu)&&$submenu=='skills')
                                    <a href="{{route('deleteMenu', encrypt($headerMenu->id))}}" title="Delete '{{$headerMenu->menu}}' Menu" class=" text-muted"
                                    data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                @endif
                            @endif
                            @if($headerMenu->menu=='Experiences')
                                <li class="nav-item @if(isset($submenu)&&$submenu=='experiences') active disabled @endif">
                                    <a class="nav-link" href="{{route('headerMenu.experiences')}}">Experiences</a>
                                </li>
                                @if(isset($submenu)&&$submenu=='experiences')
                                    <a href="{{route('deleteMenu', encrypt($headerMenu->id))}}" title="Delete '{{$headerMenu->menu}}' Menu" class=" text-muted"
                                    data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                @endif
                            @endif
                            @if($headerMenu->menu=='Portfolio')
                                <li class="nav-item @if(isset($submenu)&&$submenu=='portfolio') active disabled @endif">
                                    <a class="nav-link" href="{{route('headerMenu.portfolio')}}">Portfolio</a>
                                </li>
                                @if(isset($submenu)&&$submenu=='portfolio')
                                    <a href="{{route('deleteMenu', encrypt($headerMenu->id))}}" title="Delete '{{$headerMenu->menu}}' Menu" class=" text-muted"
                                    data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                @endif
                            @endif
                            @if($headerMenu->menu=='Pricing')
                                <li class="nav-item @if(isset($submenu)&&$submenu=='pricing') active disabled @endif">
                                    <a class="nav-link" href="{{route('headerMenu.pricing')}}">Pricing</a>
                                </li>
                                @if(isset($submenu)&&$submenu=='pricing')
                                    <a href="{{route('deleteMenu', encrypt($headerMenu->id))}}" title="Delete '{{$headerMenu->menu}}' Menu" class=" text-muted"
                                    data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                @endif
                            @endif
                            @if($headerMenu->menu=='Blog')
                                <li class="nav-item @if(isset($submenu)&&$submenu=='blog') active disabled @endif">
                                    <a class="nav-link" href="{{route('headerMenu.blog')}}">Blog</a>
                                </li>
                                @if(isset($submenu)&&$submenu=='blog')
                                    <a href="{{route('deleteMenu', encrypt($headerMenu->id))}}" title="Delete '{{$headerMenu->menu}}' Menu" class=" text-muted"
                                    data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                @endif
                            @endif
                            @if($headerMenu->menu=='Contact')
                                <li class="nav-item @if(isset($submenu)&&$submenu=='contact') active disabled @endif">
                                    <a class="nav-link" href="{{route('headerMenu.contact')}}">Contact</a>
                                </li>
                                @if(isset($submenu)&&$submenu=='contact')
                                    <a href="{{route('deleteMenu', encrypt($headerMenu->id))}}" title="Delete '{{$headerMenu->menu}}' Menu" class=" text-muted"
                                    data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                @endif
                            @endif
                        @endforeach
                        @if(!(isset($submenu)&&$submenu=='profile') && !empty(unusedHeaderMenus()['0']))
                            <div class="dropdown">
                                <div class=" align-self-end">
                                    <span id="add-menu-dropdown" title="Add Menu" class="nav-link dropdown-button cursor-pointer" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="fa fa-plus"></i> </span>

                                    <div class="dropdown-menu dropdown-menu-left dark-bg" aria-labelledby="userDropdown">
                                        @foreach(unusedHeaderMenus() as $unusedHeaderMenu)
                                            <a class="dropdown-item new-header-menu " href="{{route('addMenu', encrypt($unusedHeaderMenu->id))}}">{{$unusedHeaderMenu->menu}}</a>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endif
                    </ul>
                </div>
                <div style="margin: auto"></div>

{{--                <div class="header-part-left">--}}
{{--                    <!-- User avatar dropdown -->--}}
{{--                    <div class="dropdown">--}}
{{--                        <div class="align-self-end">--}}
{{--                            <img src="{{profilePicture()}}" height="50" width="50" class="rounded-circle dropdown-button" id="userDropdown" alt="Name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}

{{--                            <div class="dropdown-menu dropdown-menu-left dark-bg" aria-labelledby="userDropdown" style="left: -80px">--}}
{{--                                <span class="dropdown-item disabled" href="">{{$user->first_name . ' ' . $user->last_name}}</span>--}}
{{--                                <span class="dropdown-item disabled" href="">{{('@') . $user->username}}</span>--}}
{{--                                <hr>--}}
{{--                                <a class="dropdown-item" href="{{route('profile')}}">{{__('Profile')}}</a>--}}
{{--                                <a class="dropdown-item" href="{{route('signOut')}}">{{__('Sign out')}}</a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
            </nav>
        </div>
    </div>
</header>
