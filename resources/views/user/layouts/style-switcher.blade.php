
<!-- ****************
  After neccessary customization/modification, Please minify
  JavaScript/jQuery according to http://browserdiet.com/en/ for better performance
**************** -->
<!-- STYLE SWITCH STYLESHEET ONLY FOR DEMO -->
<link rel="stylesheet" href="{{asset('demo/demo.css')}}">
<script type="text/javascript" src="{{asset('demo/demo.js')}}"></script>
<div class="demo-style-switch" id="switch-style">
    <a id="toggle-switcher" class="switch-button"><i class="fa fa-snowflake-o fa-spin"></i></a>
    <div class="switched-options">
        <ul>
            <li class="mh-demo-styles">
                <div class="config-title">
                    Mode:
                </div>
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link text-dark font-weight-bold" href="{{route('account.messages')}}">My Account</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark font-weight-bold" href="{{route('customize')}}">Customization</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark font-weight-bold" href="{{route('preview')}}">Preview</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link text-dark font-weight-bold" href="{{route('demo')}}" target="_blank">Demo</a>
                    </li>
                </ul>
            </li>
        </ul>
        <ul>
            <li class="mh-demo-styles">
                <div class="config-title">
                    Style:
                </div>
                <ul>
                    <li>
                        <a title="Theme Dark" href="{{route('updateSettings', [encrypt('theme'), encrypt(THEME_DARK)])}}">
                            <img src="{{asset('assets/images/h1w.png')}}" alt="" class="img-fluid">
                        </a>
                    </li>
                    <li>
                        <a title="Theme Light" href="{{route('updateSettings', [encrypt('theme'), encrypt(THEME_LIGHT)])}}">
                            <img src="{{asset('assets/images/h1.png')}}" alt="" class="img-fluid">
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
        <div class="config-title">
            Colors :
        </div>
        <ul class="styles">
            <li><a href="{{route('updateSettings', [encrypt('fill'), encrypt(FILL_PURPLE)])}}" title="Purple"><div class="purple"></div></a>
            </li>
            <li><a href="{{route('updateSettings', [encrypt('fill'), encrypt(FILL_RED)])}}" title="Red"><div class="red"></div></a>
            </li>
            <li><a href="{{route('updateSettings', [encrypt('fill'), encrypt(FILL_BLUE)])}}" title="Blue"><div class="blue"></div></a>
            </li>
            <li><a href="{{route('updateSettings', [encrypt('fill'), encrypt(FILL_YELLOW)])}}" title="Yellow"><div class="yellow"></div></a>
            </li>
            <li><a href="{{route('updateSettings', [encrypt('fill'), encrypt(FILL_PINK)])}}" title="Pink"><div class="pink"></div></a>
            </li>
            <li><a href="{{route('updateSettings', [encrypt('fill'), encrypt(FILL_GREEN)])}}" title="Green"><div class="green"></div></a>
            </li>
            <li><a href="{{route('updateSettings', [encrypt('fill'), encrypt(FILL_DARK_GREEN)])}}" title="Dark Green"><div class="dark-green"></div></a>
            </li>
            <li><a href="{{route('updateSettings', [encrypt('fill'), encrypt(FILL_BLUE_MUNSELL)])}}" title="Blue Munsell"><div class="blue-munsell"></div></a>
            </li>
        </ul>
    </div>
</div>
