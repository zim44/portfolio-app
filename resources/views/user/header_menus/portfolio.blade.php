@extends('user.layouts.master')

@section('title')
    Recent Portfolio - Header Menu - Portfolio
@endsection

@section('content')
    <section class="mh-portfolio" id="mh-portfolio">
        <div class="container">
            <div class="row section-separator">
                <div class="section-title col-sm-12 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">
                    <h3>Recent Portfolio
                        <a title="Add Portfolio Category" id="portfolio-category-add-button" class="ml-1 text-muted" style="font-size: small"> <i class="fa fa-plus"></i> </a>
                    </h3>
                </div>
                <div class="part col-sm-12">
                    <div class="portfolio-nav col-sm-12" id="filter-button">
                        @if(count($portfolioCategories)==0)
                            <h4 class="text-info center">Add Category</h4>
                        @else
                            <ul>
                                <li data-filter="*" class="current wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s"> <span>All Categories</span></li>
                                @foreach($portfolioCategories as $portfolioCategory)
                                    <li data-filter=".{{$portfolioCategory->filter_key}}" class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                        <span>{{$portfolioCategory->title}}</span>
                                    </li>
                                    <a title="Add Portfolio Content" data-info="{{$portfolioCategory}}" class="portfolio-content-add-button ml-1 text-muted" style="font-size: small"> <i class="fa fa-plus"></i> </a>
                                    <a title="Edit {{$portfolioCategory->title}}" data-info="{{$portfolioCategory}}" class="portfolio-category-edit-button wow fadeInUp ml-1 text-muted" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                    <a title="Delete {{$portfolioCategory->title}}" href="{{route('headerMenu.deletePortfolioCategory', encrypt($portfolioCategory->id))}}" class="ml-1 text-muted" style="font-size: small"
                                       data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                    <div class="mh-project-gallery col-sm-12 wow fadeInUp" id="project-gallery" data-wow-duration="0.8s" data-wow-delay="0.5s">
                        <div class="portfolioContainer row">
                            @if(count($portfolioContents)==0)
                                <h4 class="text-info center">Add Content</h4>
                            @else
                                @foreach($portfolioContents as $portfolioContent)
                                    <div class="grid-item col-md-4 col-sm-6 col-xs-12 {{$portfolioContent->class}}">
                                        <a title="Delete" href="{{route('headerMenu.deletePortfolioCategory', encrypt($portfolioContent->id))}}" class="ml-1 text-muted float-right" style="font-size: small"
                                           data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                        <a title="Edit" data-info="{{$portfolioContent}}" class="portfolio-content-edit-button wow fadeInUp ml-1 text-muted float-right" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                        <figure>
                                            <img src="{{asset(portfolioContentImageViewPath() . $portfolioContent->image)}}" alt="img04">
                                            <figcaption class="fig-caption">
                                                <i class="fa fa-search"></i>
                                                <h5 class="title">{{$portfolioContent->title}}</h5>
                                                <span class="sub-title">{{$portfolioContent->subtitle}}</span>
                                                <a href="{{$portfolioContent->url}}" target="_blank"></a>
{{--                                                <a data-fancybox data-src="#mh"></a>--}}
                                            </figcaption>
                                        </figure>
                                    </div>
                                @endforeach
                            @endif
                        </div> <!-- End: .grid .project-gallery -->
                    </div> <!-- End: .grid .project-gallery -->
                </div> <!-- End: .part -->
            </div> <!-- End: .row -->
        </div>
        <div class="mh-portfolio-modal" id="mh">
            <div class="container">
                <div class="row mh-portfolio-modal-inner">
                    <div class="col-sm-5">
                        <h2>Wrap - A campanion plugin</h2>
                        <p>Wrap is a clean and elegant Multipurpose Landing Page Template.
                            It will fit perfectly for Startup, Web App or any type of Web Services.
                            It has 4 background styles with 6 homepage styles. 6 pre-defined color scheme.
                            All variations are organized separately so you can use / customize the template very easily.</p>

                        <p>It is a clean and elegant Multipurpose Landing Page Template.
                            It will fit perfectly for Startup, Web App or any type of Web Services.
                            It has 4 background styles with 6 homepage styles. 6 pre-defined color scheme.
                            All variations are organized separately so you can use / customize the template very easily.</p>
                        <div class="mh-about-tag">
                            <ul>
                                <li><span>php</span></li>
                                <li><span>html</span></li>
                                <li><span>css</span></li>
                                <li><span>php</span></li>
                                <li><span>wordpress</span></li>
                                <li><span>React</span></li>
                                <li><span>Javascript</span></li>
                            </ul>
                        </div>
                        <a href="#" class="btn btn-fill">Live Demo</a>
                    </div>
                    <div class="col-sm-7">
                        <div class="mh-portfolio-modal-img">
                            <img src="{{asset('assets/images/pr-0.jif')}}" alt="" class="img-fluid">
                            <p>All variations are organized separately so you can use / customize the template very easily.</p>
                            <img src="{{asset('assets/images/pr-1.jif')}}" alt="" class="img-fluid">
                            <p>All variations are organized separately so you can use / customize the template very easily.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--
    ===================
       QUATES
    ===================
    -->
    <section class="mh-quates image-bg home-2-img">
        <div class="img-color-overlay">
            <div class="container">
                <div class="row section-separator">
                    <div class="each-quates col-sm-12 col-md-6">
                        <h3 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">Interested to Work?</h3>
                        <p class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat. Mirum est notare quam littera gothica.
                            quam nunc putamus parum claram,</p>
                        <a href="#mh-contact" class="btn btn-fill wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s">Contact</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hidden Form To Add Portfolio Category -->
    <div id="add-portfolio-category-form" class="hidden float-bottom-left">
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.storePortfolioCategory', 'method' => 'post']) }}
        <div class="form-group">
            <label for="title">{{__('Category Title')}}</label>
            <input name="title" id="title" class="form-control form-control-rounded" type="text" value="{{old('title')}}" placeholder="">
            @if (isset($errors) && $errors->has('title'))
                <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Edit Portfolio Category -->
    <div id="edit-portfolio-category-form" class="hidden float-bottom-left">
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.updatePortfolioCategory', 'method' => 'post']) }}
        <input type="hidden" name="id" id="id" value="">
        <div class="form-group">
            <label for="title">{{__('Category Title')}}</label>
            <input name="title" id="title" class="form-control form-control-rounded" type="text" value="">
            @if (isset($errors) && $errors->has('title'))
                <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Add Portfolio Content -->
    <div id="add-portfolio-content-form" class="col-8 hidden float-bottom-left">
        <h5>New Portfolio Content</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.storePortfolioContent', 'method' => 'post', 'files' => 'true']) }}
        <input type="hidden" name="portfolio_category_id" id="portfolio_category_id" value="">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="title">{{__('Title')}}</label>
                    <input name="title" id="title" class="form-control form-control-rounded" type="text" value="{{old('title')}}" placeholder="Title">
                    @if (isset($errors) && $errors->has('title'))
                        <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="subtitle">{{__('Subtitle')}}</label>
                    <input name="subtitle" id="subtitle" class="form-control form-control-rounded" type="text" value="{{old('subtitle')}}" placeholder="Subtitle">
                    @if (isset($errors) && $errors->has('subtitle'))
                        <span class="text-danger"><strong>{{ $errors->first('subtitle') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="url">{{__('URL')}}</label>
                    <input name="url" id="url" class="form-control form-control-rounded" type="text" value="{{old('url')}}" placeholder="URL">
                    @if (isset($errors) && $errors->has('url'))
                        <span class="text-danger"><strong>{{ $errors->first('url') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>{{__('Image')}}</label>
                    <input name="image" type="file" id="input-file-now" class="dropify" data-default-file="">
                    @if (isset($errors) && $errors->has('image'))
                        <div class="text-danger">{{ $errors->first('image') }}</div>
                    @endif
                </div>
            </div>
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Edit Portfolio Content -->
    <div id="edit-portfolio-content-form" class="col-8 hidden float-bottom-left">
        <h5>New Portfolio Content</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.updatePortfolioContent', 'method' => 'post', 'files' => 'true']) }}
        <input type="hidden" name="id" id="id" value="">
        <input type="hidden" name="portfolio_category_id" id="portfolio_category_id" value="">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="title">{{__('Title')}}</label>
                    <input name="title" id="title" class="form-control form-control-rounded" type="text" value="">
                    @if (isset($errors) && $errors->has('title'))
                        <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="subtitle">{{__('Subtitle')}}</label>
                    <input name="subtitle" id="subtitle" class="form-control form-control-rounded" type="text" value="">
                    @if (isset($errors) && $errors->has('subtitle'))
                        <span class="text-danger"><strong>{{ $errors->first('subtitle') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="url">{{__('URL')}}</label>
                    <input name="url" id="url" class="form-control form-control-rounded" type="text" value="">
                    @if (isset($errors) && $errors->has('url'))
                        <span class="text-danger"><strong>{{ $errors->first('url') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>{{__('Image')}}</label>
                    <input name="image" type="file" id="input-file-now" class="dropify" data-default-file="">
                    @if (isset($errors) && $errors->has('image'))
                        <div class="text-danger">{{ $errors->first('image') }}</div>
                    @endif
                </div>
            </div>
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
@endsection

@section('script')
    <script type='text/javascript' src='{{ asset('assets/js/dropify.min.js') }}'></script>
    <script>
        $(document).ready(function () {
            $('.dropify').dropify();
        });
    </script>
    <script>
        $('.hidden').hide();
        $('document').ready(function () {
            $('#portfolio-category-add-button').on('click', function () {
                $('.hidden').hide();
                $('#add-portfolio-category-form').show('slow');
            });
            $('.portfolio-category-edit-button').on('click', function () {
                let info = $(this).data('info');
                $('.hidden').hide();
                $('#edit-portfolio-category-form').show('slow');
                $('#edit-portfolio-category-form').find('#id').val(info['id']);
                $('#edit-portfolio-category-form').find('#title').val(info['title']);
            });
            $('.portfolio-content-add-button').on('click', function () {
                let info = $(this).data('info');
                $('.hidden').hide();
                $('#add-portfolio-content-form').show('slow');
                $('#add-portfolio-content-form').find('#portfolio_category_id').val(info['id']);
            });
            $('.portfolio-content-edit-button').on('click', function () {
                let info = $(this).data('info');
                $('.hidden').hide();
                $('#edit-portfolio-content-form').show('slow');
                $('#edit-portfolio-content-form').find('#id').val(info['id']);
                $('#edit-portfolio-content-form').find('#portfolio_category_id').val(info['portfolio_category_id']);
                $('#edit-portfolio-content-form').find('#title').val(info['title']);
                $('#edit-portfolio-content-form').find('#subtitle').val(info['subtitle']);
                $('#edit-portfolio-content-form').find('#url').val(info['url']);
            });
        });
    </script>
@endsection
