@extends('user.layouts.master')

@section('title')
    Blog - Header Menu - Portfolio
@endsection

@section('content')
    <section class="mh-blog image-bg featured-img-two" id="mh-blog">
        <div class="img-color-overlay">
            <div class="container">
                <div class="row section-separator">
                    <div class="col-sm-12 section-title wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">
                        <h3>Featured Posts
                            <a title="Add Post" id="post-add-button" class="ml-1 text-muted" style="font-size: small"> <i class="fa fa-plus"></i> </a>
                        </h3>
                    </div>
                    @if(count($posts)==0)
                        <h4 class="text-info center">No Post Added</h4>
                    @else
                        @foreach($posts as $post)
                            <div class="col-sm-12 col-md-4">
                                <div class="mh-blog-item dark-bg wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                    <img src="{{asset(blogImageViewPath().$post->image)}}" alt="" class="img-fluid">
                                    <div class="blog-inner">
                                        <h2><a href="{{$post->url}}" target="_blank">{{$post->title}}</a></h2>
                                        <div class="mh-blog-post-info">
                                            <ul>
                                                <li><strong>Post On</strong><a href="#">{{date_format($post->created_at, 'd.m.y')}}</a></li>
                                                <li><strong>By</strong><a href="#">{{$post->site}}</a></li>
                                            </ul>
                                        </div>
                                        <p>{{substr($post->content, 0, 200)}}</p>
                                        <a href="{{$post->url}}" target="_blank">Read More</a>

                                        <a title="Delete Post" href="{{route('headerMenu.deletePost', encrypt($post->id))}}" class="post-delete-button ml-1 text-muted float-right" style="font-size: small"
                                           data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                        <a title="Edit Post" data-info="{{$post}}" class="post-edit-button ml-1 text-muted float-right" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </section>
    <!-- Hidden Form To Add Post -->
    <div id="add-post-form" class="col-8 hidden float-bottom-left overflow-scroll-y">
        <h5>New Post</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.storePost', 'method' => 'post', 'files' => 'true']) }}
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="title">{{__('Title')}}</label>
                    <input name="title" id="title" class="form-control form-control-rounded" type="text" value="{{old('title')}}" placeholder="">
                    @if (isset($errors) && $errors->has('title'))
                        <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="site">{{__('Site Name')}}</label>
                    <input name="site" id="site" class="form-control form-control-rounded" type="text" value="{{old('site')}}" placeholder="">
                    @if (isset($errors) && $errors->has('site'))
                        <span class="text-danger"><strong>{{ $errors->first('site') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="url">{{__('URL')}}</label>
                    <input name="url" id="url" class="form-control form-control-rounded" type="text" value="{{old('url')}}" placeholder="">
                    @if (isset($errors) && $errors->has('url'))
                        <span class="text-danger"><strong>{{ $errors->first('url') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>{{__('Image')}}</label>
                    <input name="image" type="file" id="input-file-now" class="dropify" data-default-file="">
                    @if (isset($errors) && $errors->has('image'))
                        <div class="text-danger">{{ $errors->first('image') }}</div>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="content">{{__('Content')}}</label>
            <textarea name="content" id="content" class="form-control form-control-rounded" type="text">{{old('content')}}</textarea>
            @if (isset($errors) && $errors->has('content'))
                <span class="text-danger"><strong>{{ $errors->first('content') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Edit Post -->
    <div id="edit-post-form" class="col-8 hidden float-bottom-left overflow-scroll-y">
        <h5>Edit Post</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.updatePost', 'method' => 'post', 'files' => 'true']) }}
        <div class="row">
            <input type="hidden" name="id" id="id" value="">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="title">{{__('Title')}}</label>
                    <input name="title" id="title" class="form-control form-control-rounded" type="text" value="" placeholder="">
                    @if (isset($errors) && $errors->has('title'))
                        <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="site">{{__('Site Name')}}</label>
                    <input name="site" id="site" class="form-control form-control-rounded" type="text" value="" placeholder="">
                    @if (isset($errors) && $errors->has('site'))
                        <span class="text-danger"><strong>{{ $errors->first('site') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="url">{{__('URL')}}</label>
                    <input name="url" id="url" class="form-control form-control-rounded" type="text" value="" placeholder="">
                    @if (isset($errors) && $errors->has('url'))
                        <span class="text-danger"><strong>{{ $errors->first('url') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>{{__('Image')}}</label>
                    <input name="image" type="file" id="input-file-now" class="dropify" data-default-file="">
                    @if (isset($errors) && $errors->has('image'))
                        <div class="text-danger">{{ $errors->first('image') }}</div>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="content">{{__('Content')}}</label>
            <textarea name="content" id="content" class="form-control form-control-rounded" type="text"></textarea>
            @if (isset($errors) && $errors->has('content'))
                <span class="text-danger"><strong>{{ $errors->first('content') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
@endsection

@section('script')
    <script type='text/javascript' src='{{ asset('assets/js/dropify.min.js') }}'></script>
    <script>
        $(document).ready(function () {
            $('.dropify').dropify();
        });
    </script>
    <script>
        $('.hidden').hide();
        $('document').ready(function () {
            $('#post-add-button').on('click', function () {
                $('.hidden').hide();
                $('#add-post-form').show('slow');
            });
            $('.post-edit-button').on('click', function () {
                let info = $(this).data('info');
                $('.hidden').hide();
                $('#edit-post-form').show('slow');
                $('#edit-post-form').find('#id').val(info['id']);
                $('#edit-post-form').find('#title').val(info['title']);
                $('#edit-post-form').find('#site').val(info['site']);
                $('#edit-post-form').find('#url').val(info['url']);
                $('#edit-post-form').find('#content').html(info['content']);
            });
        });
    </script>
@endsection
