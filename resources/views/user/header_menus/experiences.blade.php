@extends('user.layouts.master')

@section('title')
    Experiences - Header Menu - Portfolio
@endsection

@section('content')
    <section class="mh-experince image-bg featured-img-one" id="mh-experience">
        <div class="img-color-overlay">
            <div class="container">
                <div class="row section-separator mt-5">
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-education">
                            <h3 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">Education
                                <a title="Add Educational Experience" id="educational-experience-add-button" class="ml-1 text-muted" style="font-size: small"> <i class="fa fa-plus"></i> </a>
                            </h3>
                            <div class="mh-education-deatils">
                                @if(count($educations)==0)
                                    <h4 class="text-info">Not added</h4>
                                @else
                                    @foreach($educations as $education)
                                        <!-- Education Institutes-->
                                        <div class="mh-education-item dark-bg wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                            <h4>{{$education->subject}} From <a href="{{$education->url}}" target="_blank">{{$education->institution}}</a></h4>
                                            <div class="mh-eduyear">{{$education->year_started}}-{{$education->year_ended}}
                                                <a title="Delete Educational Experience" href="{{route('headerMenu.deleteEducation', encrypt($education->id))}}" class="price-delete-button ml-1 text-muted float-right mr-2" style="font-size: small"
                                                   data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                                <a title="Edit Educational Experience" data-info="{{$education}}" class="educational-experience-edit-button ml-1 text-muted float-right" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                            </div>
                                            <p>{{$education->description}}</p>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-work">
                            <h3>Work Experience
                                <a title="Add Work Experience" id="work-experience-add-button" class="ml-1 text-muted" style="font-size: small"> <i class="fa fa-plus"></i> </a>
                            </h3>
                            <div class="mh-experience-deatils">
                                @if(count($workExperiences)==0)
                                    <h4 class="text-info">Not added</h4>
                                @else
                                    @foreach($workExperiences as $workExperience)
                                        <!-- Work Experience-->
                                        <div class="mh-work-item dark-bg wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s">
                                            <h4>{{$workExperience->job_position}} <a href="{{$workExperience->url}}">{{$workExperience->company}}</a></h4>
                                            <div class="mh-eduyear">{{$workExperience->year_started}}-{{$workExperience->year_ended}}
                                                <a title="Delete Work Experience" href="{{route('headerMenu.deleteWorkExperience', encrypt($workExperience->id))}}" class="price-delete-button ml-1 text-muted float-right mr-2" style="font-size: small"
                                                   data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                                <a title="Edit Work Experience" data-info="{{$workExperience}}" class="work-experience-edit-button ml-1 text-muted float-right" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                            </div>
                                            <span>Responsibility :</span>
                                            <ul class="work-responsibility">
                                                @foreach($workExperience->responsibilities as $responsibility)
                                                    <li><i class="fa fa-circle"></i>{{$responsibility}}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Hidden Form To Add Educational Experience -->
    <div id="add-educational-experience-form" class="col-8 hidden float-bottom-left overflow-scroll-y">
        <h5>New Educational Experience</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.storeEducation', 'method' => 'post']) }}
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="subject">{{__('Subject')}}</label>
                    <input name="subject" id="subject" class="form-control form-control-rounded" type="text" value="{{old('subject')}}" placeholder="">
                    @if (isset($errors) && $errors->has('subject'))
                        <span class="text-danger"><strong>{{ $errors->first('subject') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="institution">{{__('Institution')}}</label>
                    <input name="institution" id="institution" class="form-control form-control-rounded" type="text" value="{{old('institution')}}" placeholder="">
                    @if (isset($errors) && $errors->has('institution'))
                        <span class="text-danger"><strong>{{ $errors->first('institution') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>{{__('Started At')}}</label>
                    <input name="started_at" id="started_at" class="form-control form-control-rounded" type="date" value="{{old('started_at')}}" placeholder="">
                    @if (isset($errors) && $errors->has('started_at'))
                        <span class="text-danger"><strong>{{ $errors->first('started_at') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label>{{__('Ended At')}}</label>
                    <input name="ended_at" id="ended_at" class="form-control form-control-rounded" type="date" value="{{old('ended_at')}}" placeholder="">
                    @if (isset($errors) && $errors->has('ended_at'))
                        <span class="text-danger"><strong>{{ $errors->first('ended_at') }}</strong></span>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="url">{{__('URL')}}</label>
            <input name="url" id="url" class="form-control form-control-rounded" type="text" value="{{old('url')}}" placeholder="">
            @if (isset($errors) && $errors->has('url'))
                <span class="text-danger"><strong>{{ $errors->first('url') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="description">{{__('Description')}}</label>
            <textarea name="description" id="description" class="form-control form-control-rounded" placeholder="Add comma(,) separated contents" type="text">{{old('description')}}</textarea>
            @if (isset($errors) && $errors->has('description'))
                <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Edit Educational Experience -->
    <div id="edit-educational-experience-form" class="col-8 hidden float-bottom-left overflow-scroll-y">
        <h5>Edit Educational Experience</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.updateEducation', 'method' => 'post']) }}
        <input type="hidden" name="id" id="id" value="">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="subject">{{__('Subject')}}</label>
                    <input name="subject" id="subject" class="form-control form-control-rounded" type="text" value="">
                    @if (isset($errors) && $errors->has('subject'))
                        <span class="text-danger"><strong>{{ $errors->first('subject') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="institution">{{__('Institution')}}</label>
                    <input name="institution" id="institution" class="form-control form-control-rounded" type="text" value="">
                    @if (isset($errors) && $errors->has('institution'))
                        <span class="text-danger"><strong>{{ $errors->first('institution') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>{{__('Started At')}}</label>
                    <input name="started_at" id="started_at" class="form-control form-control-rounded" type="date" value="">
                    @if (isset($errors) && $errors->has('started_at'))
                        <span class="text-danger"><strong>{{ $errors->first('started_at') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label>{{__('Ended At')}}</label>
                    <input name="ended_at" id="ended_at" class="form-control form-control-rounded" type="date" value="">
                    @if (isset($errors) && $errors->has('ended_at'))
                        <span class="text-danger"><strong>{{ $errors->first('ended_at') }}</strong></span>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="url">{{__('URL')}}</label>
            <input name="url" id="url" class="form-control form-control-rounded" type="text" value="">
            @if (isset($errors) && $errors->has('url'))
                <span class="text-danger"><strong>{{ $errors->first('url') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="description">{{__('Description')}}</label>
            <textarea name="description" id="description" class="form-control form-control-rounded" type="text"></textarea>
            @if (isset($errors) && $errors->has('description'))
                <span class="text-danger"><strong>{{ $errors->first('description') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Add Work Experience -->
    <div id="add-work-experience-form" class="col-8 hidden float-bottom-right overflow-scroll-y">
        <h5>New Work Experience</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.storeWorkExperience', 'method' => 'post']) }}
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="job_position">{{__('Job Position')}}</label>
                    <input name="job_position" id="job_position" class="form-control form-control-rounded" type="text" value="{{old('job_position')}}" placeholder="">
                    @if (isset($errors) && $errors->has('job_position'))
                        <span class="text-danger"><strong>{{ $errors->first('job_position') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="company">{{__('Company Name')}}</label>
                    <input name="company" id="company" class="form-control form-control-rounded" type="text" value="{{old('company')}}" placeholder="">
                    @if (isset($errors) && $errors->has('company'))
                        <span class="text-danger"><strong>{{ $errors->first('company') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>{{__('Started At')}}</label>
                    <input name="started_at" id="started_at" class="form-control form-control-rounded" type="date" value="{{old('started_at')}}" placeholder="">
                    @if (isset($errors) && $errors->has('started_at'))
                        <span class="text-danger"><strong>{{ $errors->first('started_at') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label>{{__('Ended At')}}</label>
                    <input name="ended_at" id="ended_at" class="form-control form-control-rounded" type="date" value="{{old('ended_at')}}" placeholder="">
                    @if (isset($errors) && $errors->has('ended_at'))
                        <span class="text-danger"><strong>{{ $errors->first('ended_at') }}</strong></span>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="url">{{__('Company URL')}}<span>(Recommended. Enter (#) if no url)</span></label>
            <input name="url" id="url" class="form-control form-control-rounded" type="text" value="{{old('url')}}" placeholder="">
            @if (isset($errors) && $errors->has('url'))
                <span class="text-danger"><strong>{{ $errors->first('url') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="responsibility">{{__('Responsibilities')}}<span>(Add comma(,) separated contents)</span></label>
            <textarea name="responsibility" id="responsibility" class="form-control form-control-rounded" placeholder="Responsibilities" type="text">{{old('responsibility')}}</textarea>
            @if (isset($errors) && $errors->has('responsibility'))
                <span class="text-danger"><strong>{{ $errors->first('responsibility') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Edit Work Experience -->
    <div id="edit-work-experience-form" class="col-8 hidden float-bottom-right overflow-scroll-y">
        <h5>Edit Work Experience</h5>
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.updateWorkExperience', 'method' => 'post']) }}
        <input type="hidden" name="id" id="id" value="">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label for="job_position">{{__('Job Position')}}</label>
                    <input name="job_position" id="job_position" class="form-control form-control-rounded" type="text" value="">
                    @if (isset($errors) && $errors->has('job_position'))
                        <span class="text-danger"><strong>{{ $errors->first('job_position') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="company">{{__('Company Name')}}</label>
                    <input name="company" id="company" class="form-control form-control-rounded" type="text" value="">
                    @if (isset($errors) && $errors->has('company'))
                        <span class="text-danger"><strong>{{ $errors->first('company') }}</strong></span>
                    @endif
                </div>
            </div>
            <div class="col-md-6 col-sm-12">
                <div class="form-group">
                    <label>{{__('Started At')}}</label>
                    <input name="started_at" id="started_at" class="form-control form-control-rounded" type="date" value="">
                    @if (isset($errors) && $errors->has('started_at'))
                        <span class="text-danger"><strong>{{ $errors->first('started_at') }}</strong></span>
                    @endif
                </div>
                <div class="form-group">
                    <label>{{__('Ended At')}}</label>
                    <input name="ended_at" id="ended_at" class="form-control form-control-rounded" type="date" value="">
                    @if (isset($errors) && $errors->has('ended_at'))
                        <span class="text-danger"><strong>{{ $errors->first('ended_at') }}</strong></span>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="url">{{__('Company URL')}}<span>(Recommended. Enter (#) if no url)</span></label>
            <input name="url" id="url" class="form-control form-control-rounded" type="text" value="">
            @if (isset($errors) && $errors->has('url'))
                <span class="text-danger"><strong>{{ $errors->first('url') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <label for="responsibility">{{__('Responsibilities')}}<span>(Add comma(,) separated contents)</span></label>
            <textarea name="responsibility" id="responsibility" class="form-control form-control-rounded" placeholder="Responsibilities" type="text"></textarea>
            @if (isset($errors) && $errors->has('responsibility'))
                <span class="text-danger"><strong>{{ $errors->first('responsibility') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
@endsection

@section('script')
    <script>
        $('.hidden').hide();
        $('document').ready(function () {
            $('#educational-experience-add-button').on('click', function () {
                $('.hidden').hide();
                $('#add-educational-experience-form').show('slow');
            });
            $('.educational-experience-edit-button').on('click', function () {
                let info = $(this).data('info');
                $('.hidden').hide();
                $('#edit-educational-experience-form').show('slow');
                $('#edit-educational-experience-form').find('#id').val(info['id']);
                $('#edit-educational-experience-form').find('#subject').val(info['subject']);
                $('#edit-educational-experience-form').find('#institution').val(info['institution']);
                $('#edit-educational-experience-form').find('#started_at').val(info['started_at']);
                $('#edit-educational-experience-form').find('#ended_at').val(info['ended_at']);
                $('#edit-educational-experience-form').find('#url').val(info['url']);
                $('#edit-educational-experience-form').find('#description').html(info['description']);
            });
            $('#work-experience-add-button').on('click', function () {
                $('.hidden').hide();
                $('#add-work-experience-form').show('slow');
            });
            $('.work-experience-edit-button').on('click', function () {
                let info = $(this).data('info');
                $('.hidden').hide();
                $('#edit-work-experience-form').show('slow');
                $('#edit-work-experience-form').find('#id').val(info['id']);
                $('#edit-work-experience-form').find('#job_position').val(info['job_position']);
                $('#edit-work-experience-form').find('#company').val(info['company']);
                $('#edit-work-experience-form').find('#started_at').val(info['started_at']);
                $('#edit-work-experience-form').find('#ended_at').val(info['ended_at']);
                $('#edit-work-experience-form').find('#url').val(info['url']);
                $('#edit-work-experience-form').find('#responsibility').html(info['responsibility']);
            });
        });
    </script>
@endsection
