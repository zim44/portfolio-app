@extends('user.layouts.master')

@section('title')
    Skills - Header Menu - Portfolio
@endsection

@section('content')
    <section class="mh-skills" id="mh-skills">
        <div class="home-v-img mt-5">
            <div class="container">
                <div class="row section-separator">
                    <div class="section-title text-center col-sm-12">
{{--                        <h2>Skills</h2>--}}
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-skills-inner">
                            <div class="mh-professional-skill wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">
                                <h3 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">Technical Skills
                                    <a title="Add Technical Skill" id="technical-skill-add-button" class="ml-1 text-muted" style="font-size: small"> <i class="fa fa-plus"></i> </a>
                                </h3>
                                <div class="each-skills">
                                    @if(count($technicalSkills)==0)
                                        <h4 class="text-info center">No Skill added</h4>
                                    @else
                                        @foreach($technicalSkills as $technicalSkill)
                                            <div class="candidatos">
                                                <div class="parcial">
                                                    <div class="info">
                                                        <div class="nome">{{$technicalSkill->title}}
                                                            <a title="Edit {{$technicalSkill->title}}" data-info="{{$technicalSkill}}" class="technical-skill-edit-button ml-1 text-muted" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                                            <a title="Delete {{$technicalSkill->title}}" href="{{route('headerMenu.deleteTechnicalSkill', encrypt($technicalSkill->id))}}" class="technical-skill-delete-button ml-1 text-muted" style="font-size: small"
                                                               data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                                        </div>
                                                        <div class="percentagem-num">{{$technicalSkill->percentage}}%</div>
                                                    </div>
                                                    <div class="progressBar">
                                                        <div class="percentagem" style="width: {{$technicalSkill->percentage}}%;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6">
                        <div class="mh-professional-skills wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s">
                            <h3 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">Professional Skills
                                <a title="Add Professional Skill" id="professional-skill-add-button" class="ml-1 text-muted" style="font-size: small"> <i class="fa fa-plus"></i> </a>
                            </h3>
                            <ul class="mh-professional-progress">
                                @if(count($professionalSkills)==0)
                                    <h4 class="text-info center">No Skill added</h4>
                                @else
                                    @foreach($professionalSkills as $professionalSkill)
                                        <li>
                                            <div class="mh-progress mh-progress-circle" data-progress="{{$professionalSkill->percentage}}"></div>
                                            <div class="pr-skill-name">{{$professionalSkill->title}}
                                                <a title="Edit {{$professionalSkill->title}}" data-info="{{$professionalSkill}}" class="professional-skill-edit-button ml-1 text-muted" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                                                <a title="Delete {{$professionalSkill->title}}" href="{{route('headerMenu.deleteProfessionalSkill', encrypt($professionalSkill->id))}}" class="professional-skill-delete-button ml-1 text-muted" style="font-size: small"
                                                   data-toggle="tooltip" onclick="return confirm('Are you sure to delete this ?');"> <i class="fa fa-trash"></i> </a>
                                            </div>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Hidden Form To Add Technical Skill -->
    <div id="add-technical-skill-form" class="hidden float-bottom-left">
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.storeTechnicalSkill', 'method' => 'post']) }}
        <div class="form-group">
            <label for="title">New Technical Skill</label>
            <input name="title" id="title" class="form-control form-control-rounded" type="text" value="{{old('title')}}" placeholder="Skill Title">
            @if (isset($errors) && $errors->has('title'))
                <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <input name="percentage" id="slider1" class="border-0 fill-success w-75" type="range" min="0" max="100" />
            <span class="font-weight-bold text-success ml-2 mt-1 valueSpan1"></span>
            @if (isset($errors) && $errors->has('percentage'))
                <span class="text-danger"><strong>{{ $errors->first('percentage') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill wow btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Add Professional Skill -->
    <div id="add-professional-skill-form" class="hidden float-bottom-right">
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.storeProfessionalSkill', 'method' => 'post']) }}
        <div class="form-group">
            <label for="title">New Professional Skill</label>
            <input name="title" id="title" class="form-control form-control-rounded" type="text" value="{{old('title')}}" placeholder="Skill Title">
            @if (isset($errors) && $errors->has('title'))
                <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <input name="percentage" id="slider2" class="border-0 fill-success w-75" type="range" min="0" max="100" />
            <span class="font-weight-bold text-success ml-2 mt-1 valueSpan2"></span>
            @if (isset($errors) && $errors->has('percentage'))
                <span class="text-danger"><strong>{{ $errors->first('percentage') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill wow btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Edit Technical Skill -->
    <div id="edit-technical-skill-form" class="hidden float-bottom-left">
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.updateTechnicalSkill', 'method' => 'post']) }}
        <div class="form-group">
            <label for="title">Edit Technical Skill</label>
            <input type="hidden" name="id" id="id" value="">
            <input name="title" id="title" class="form-control form-control-rounded" type="text"  value="" >
            @if (isset($errors) && $errors->has('title'))
                <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <input name="percentage" id="slider3" class="border-0 fill-success w-75" type="range" min="0" max="100"  value=""  />
            <span class="font-weight-bold text-success ml-2 mt-1 valueSpan3"></span>
            @if (isset($errors) && $errors->has('percentage'))
                <span class="text-danger"><strong>{{ $errors->first('percentage') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill wow btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
    <!-- Hidden Form To Edit Professional Skill -->
    <div id="edit-professional-skill-form" class="hidden float-bottom-right">
        <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
        {{ Form::open(['route' => 'headerMenu.updateProfessionalSkill', 'method' => 'post']) }}
        <div class="form-group">
            <label for="title">Edit Professional Skill</label>
            <input type="hidden" name="id" id="id" value="">
            <input name="title" id="title" class="form-control form-control-rounded" type="text"  value="" >
            @if (isset($errors) && $errors->has('title'))
                <span class="text-danger"><strong>{{ $errors->first('title') }}</strong></span>
            @endif
        </div>
        <div class="form-group">
            <input name="percentage" id="slider4" class="border-0 fill-success w-75" type="range" min="0" max="100"  value=""  />
            <span class="font-weight-bold text-success ml-2 mt-1 valueSpan4"></span>
            @if (isset($errors) && $errors->has('percentage'))
                <span class="text-danger"><strong>{{ $errors->first('percentage') }}</strong></span>
            @endif
        </div>
        <button class="btn btn-fill wow btn-block mt-2">{{__('Save')}}</button>
        {{ Form::close() }}
    </div>
@endsection

@section('script')
    <script>
        $('.hidden').hide();
        $('document').ready(function () {
            $('#technical-skill-add-button').on('click', function () {
                $('.hidden').hide();
                $('#add-technical-skill-form').show('slow');
            });
            $('#professional-skill-add-button').on('click', function () {
                $('.hidden').hide();
                $('#add-professional-skill-form').show('slow');
            });
            $('.technical-skill-edit-button').on('click', function () {
                let info = $(this).data('info');
                $('.hidden').hide();
                $('#edit-technical-skill-form').show('slow');
                $('#edit-technical-skill-form').find('#id').val(info['id']);
                $('#edit-technical-skill-form').find('#title').val(info['title']);
                $('#edit-technical-skill-form').find('#percentage').val(info['percentage']);
                $('#slider3').val(info['percentage']);
                $('.valueSpan3').html(info['percentage']);
            });
            $('.professional-skill-edit-button').on('click', function () {
                let info = $(this).data('info');
                $('.hidden').hide();
                $('#edit-professional-skill-form').show('slow');
                $('#edit-professional-skill-form').find('#id').val(info['id']);
                $('#edit-professional-skill-form').find('#title').val(info['title']);
                $('#edit-professional-skill-form').find('#percentage').val(info['percentage']);
                $('#slider4').val(info['percentage']);
                $('.valueSpan4').html(info['percentage']);
            });
            let $valueSpan1 = $('.valueSpan1');
            let $value1 = $('#slider1');
            $valueSpan1.html($value1.val());
            $value1.on('input change', () => {
                $valueSpan1.html($value1.val());
            });
            let $valueSpan2 = $('.valueSpan2');
            let $value2 = $('#slider2');
            $valueSpan2.html($value2.val());
            $value2.on('input change', () => {
                $valueSpan2.html($value2.val());
            });
            let $valueSpan3 = $('.valueSpan3');
            let $value3 = $('#slider3');
            $valueSpan3.html($value3.val());
            $value3.on('input change', () => {
                $valueSpan3.html($value3.val());
            });
            let $valueSpan4 = $('.valueSpan4');
            let $value4 = $('#slider4');
            $valueSpan4.html($value4.val());
            $value4.on('input change', () => {
                $valueSpan4.html($value4.val());
            });
        });
    </script>
@endsection
