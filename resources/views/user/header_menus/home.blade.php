@extends('user.layouts.master')

@section('title')
    Home - Header Menu - Portfolio
@endsection

@section('content')
<section class="mh-home image-bg home-2-img" id="mh-home">
    <div class="img-foverlay img-color-overlay">
        <div class="container">
            <div class="row section-separator xs-column-reverse vertical-middle-content home-padding">
                <div class="col-sm-6">
                    <div class="mh-header-info">
                        <div class="mh-promo wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.1s">
                            <span>Hello I'm</span>
                        </div>

                        <h2 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.2s">{{$user->first_name.' '.$user->last_name}}
                            <a title="Edit on profile" class="text-muted" href="{{route('profile')}}" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                        </h2>
                        <div id="profession-title">
                            <h4 class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.3s">@if(isset($profession->title)) {{$profession->title}} @else Your Profession @endif
                            <a title="Edit here" id="profession-edit-button" class="ml-1 text-muted" style="font-size: small"> <i class="fa fa-edit"></i> </a></h4>
                        </div>
                        <ul>
                            <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s"><i class="fa fa-envelope"></i><a href="mailto:">{{$user->email}}</a>
                                <a title="Edit on profile" class="text-muted" href="{{route('profile')}}" style="font-size: small"> <i class="fa fa-edit"></i> </a></li>
                            <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.5s"><i class="fa fa-phone"></i><a href="callto:">
                                   @if(isset($user->phone_code)&&isset($user->phone)){{$user->phone_code.$user->phone}}
                                   @else Add Phone Number. @endif <a title="Edit on profile" class="text-muted" href="{{route('profile')}}" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                               </a></li>
                            <li class="wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.6s"><i class="fa fa-map-marker"></i><address>
                                   @if(isset($user->address)&&isset($user->city)){{$user->address}}, {{$user->city}}.
                                   @else Add Address and city name. @endif <a title="Edit on profile" class="text-muted" href="{{route('profile')}}" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                               </address></li>
                        </ul>

                        <ul class="social-icon wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.7s">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-github"></i></a></li>
                            <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="hero-img wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.6s">
                        <div class="img-border">
                            <img src="{{profilePicture()}}" alt=""  class="h-100 w-100 img-fluid">
                        </div>
                        <a title="Edit on profile" class="text-muted" href="{{route('profile')}}" style="font-size: small"> <i class="fa fa-edit"></i> </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Hidden Form to Edit Profession -->
<div id="profession-form" class="col-md-6 col-sm-10 hidden float-bottom-left">
    <span class="cursor-pointer float-right close-content"> <i class="fa fa-times"></i> </span>
    {{ Form::open(['route' => 'headerMenu.updateProfession', 'method' => 'post']) }}
    <div class="form-group">
        <label for="profession">Profession</label>
        <input name="profession" id="profession" class="form-control form-control-rounded" type="text" @if(isset($profession->title)) value="{{$profession->title}}" @else value="{{old('profession')}}" @endif>
        @if (isset($errors) && $errors->has('profession'))
            <span class="text-danger"><strong>{{ $errors->first('profession') }}</strong></span>
        @endif
    </div>
    <button class="btn btn-fill btn-block mt-2">{{__('Save')}}</button>
    {{ Form::close() }}
</div>
@endsection

@section('script')
    <script>
        $('.hidden').hide();
        $('document').ready(function () {
            $('#profession-edit-button').on('click', function () {
                $('#profession-form').show('slow');
            });
        });
    </script>
@endsection
