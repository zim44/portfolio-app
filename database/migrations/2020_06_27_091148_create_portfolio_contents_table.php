<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePortfolioContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolio_contents', function (Blueprint $table) {
            $table->id();
            $table->foreignId('portfolio_category_id');
            $table->string('title');
            $table->string('subtitle');
            $table->string('url');
            $table->string('image');
            $table->timestamps();
            $table->foreign('portfolio_category_id')->references('id')->on('portfolio_categories')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolio_contents');
    }
}
