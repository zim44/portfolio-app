<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Tony',
            'last_name' => 'Stark',
            'username' => 'tony66',
            'photo' => '5ef877386b190.jpg',
            'email' => 'tonystark66@gmail.com',
            'password' => Hash::make('123456789'),
            'verification_status' => true,
            'phone_code' => '+1',
            'phone' => '6781367092',
            'is_phone_verified' => PENDING_STATUS,
            'address' => '10880 Malibu Point',
            'zip_code' => '90265',
            'city' => 'New York',
            'country' => 'USA',
            'role' => USER_ROLE,
        ]);
    }
}
