<?php

use App\Models\ProfessionalSkill;
use Illuminate\Database\Seeder;

class ProfessionalSkillSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \App\User::where('username', 'tony66')->first()->id;
        ProfessionalSkill::create([
            'user_id' => $userId,
            'title' => 'Communication',
            'percentage' => '95',
        ]);
        ProfessionalSkill::create([
            'user_id' => $userId,
            'title' => 'Team Work',
            'percentage' => '90',
        ]);
        ProfessionalSkill::create([
            'user_id' => $userId,
            'title' => 'Innovation',
            'percentage' => '96',
        ]);
    }
}
