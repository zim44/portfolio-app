<?php

use App\Models\WorkExperience;
use Illuminate\Database\Seeder;

class WorkExperienceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \App\User::where('username', 'tony66')->first()->id;
        WorkExperience::create([
            'user_id' => $userId,
            'job_position' => 'CEO',
            'company' => 'Stark Industries',
            'url' => 'https://www.youtube.com',
            'responsibility' => 'Weapon Design, Weapon Development, Weapon Manufacturing',
            'started_at' => '2020-06-04',
            'ended_at' => '2020-06-16'
        ]);
    }
}
