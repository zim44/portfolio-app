<?php

use App\Models\UserProfession;
use Illuminate\Database\Seeder;

class UserProfessionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \App\User::where('username', 'tony66')->first()->id;
        UserProfession::create(['user_id' => $userId, 'title' => 'Iron Man']);
    }
}
