<?php

use App\Models\UserSettings;
use Illuminate\Database\Seeder;

class UserSettingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \App\User::where('username', 'tony66')->first()->id;
        UserSettings::create([
            'user_id' => $userId,
            'slug' => 'theme',
            'value' => THEME_LIGHT
        ]);
        UserSettings::create([
            'user_id' => $userId,
            'slug' => 'fill',
            'value' => FILL_DARK_GREEN
        ]);
    }
}
