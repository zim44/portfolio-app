<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
         $this->call(HeaderMenuSeeder::class);
         $this->call(UserHeaderMenuSeeder::class);
         $this->call(UserProfessionSeeder::class);
         $this->call(UserSettingsSeeder::class);
         $this->call(AboutSeeder::class);
         $this->call(UserActivitiesSeeder::class);
         $this->call(UserFeaturedProjectSeeder::class);
         $this->call(UserFeaturedProjectCommentSeeder::class);
         $this->call(TechnicalSkillSeeder::class);
         $this->call(ProfessionalSkillSeeder::class);
         $this->call(EducationSeeder::class);
         $this->call(WorkExperienceSeeder::class);
         $this->call(PortfolioCategorySeeder::class);
         $this->call(PortfolioContentSeeder::class);
         $this->call(PriceSeeder::class);
         $this->call(PostSeeder::class);
    }
}
