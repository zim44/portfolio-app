<?php

use App\Models\UserFeaturedProject;
use Illuminate\Database\Seeder;

class UserFeaturedProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \App\User::where('username', 'tony66')->first()->id;
        UserFeaturedProject::create([
            'user_id' => $userId,
            'project_type' => 'AI',
            'title' => 'JARVIS',
            'subtitle' => 'Personal Assistant',
            'description' => 'JARVIS is an AI which is made for fantasy.  JARVIS is an AI which is made for fantasy.  JARVIS is an AI which is made for fantasy.  JARVIS is an AI which is made for fantasy.  JARVIS is an AI which is made for fantasy.',
            'image' => '5ef8780421d52.jpg',
            'url' => 'https://www.youtube.com',
        ]);
        UserFeaturedProject::create([
            'user_id' => $userId,
            'project_type' => 'AI',
            'title' => 'FRIDAY',
            'subtitle' => 'Personal Assistant',
            'description' => 'FRIDAY is an AI which is made for fantasy.  FRIDAY is an AI which is made for fantasy.  FRIDAY is an AI which is made for fantasy.  FRIDAY is an AI which is made for fantasy.',
            'image' => '5ef878a8828fa.jpg',
            'url' => 'https://www.youtube.com',
        ]);
    }
}
