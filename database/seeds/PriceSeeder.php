<?php

use App\Models\Price;
use Illuminate\Database\Seeder;

class PriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userId = \App\User::where('username', 'tony66')->first()->id;
        Price::create([
            'user_id' => $userId,
            'title' => 'Weapon Production',
            'subtitle' => 'Development',
            'url' => 'https://www.youtube.com',
            'amount' => '15000',
            'content' => 'Missile, Tank, Machine gun',
            'status' => ACTIVE
        ]);
        Price::create([
            'user_id' => $userId,
            'title' => 'AI',
            'subtitle' => 'Development',
            'url' => 'https://www.youtube.com',
            'amount' => '5000',
            'content' => 'Design, Development, Deployment',
            'status' => INACTIVE
        ]);
    }
}
