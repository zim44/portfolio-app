<?php

use App\Models\HeaderMenu;
use Illuminate\Database\Seeder;

class HeaderMenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        HeaderMenu::create(['menu' => 'Home']);
        HeaderMenu::create(['menu' => 'About']);
        HeaderMenu::create(['menu' => 'Skills']);
        HeaderMenu::create(['menu' => 'Experiences']);
        HeaderMenu::create(['menu' => 'Portfolio']);
        HeaderMenu::create(['menu' => 'Pricing']);
        HeaderMenu::create(['menu' => 'Blog']);
        HeaderMenu::create(['menu' => 'Contact']);
    }
}
