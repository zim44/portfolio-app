<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFeaturedProjectComment extends Model
{
    protected $fillable = ['user_featured_project_id', 'person', 'comment'];
}
