<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SharableLinkAccessKey extends Model
{
    protected $fillable = ['user_id', 'access_key', 'status'];
}
