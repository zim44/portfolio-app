<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFeaturedProject extends Model
{
    protected $fillable = ['user_id', 'project_type', 'title', 'subtitle', 'description', 'image', 'url'];
}
