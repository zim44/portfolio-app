<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PortfolioContent extends Model
{
    protected $fillable = ['portfolio_category_id', 'title', 'subtitle', 'url', 'image'];
}
