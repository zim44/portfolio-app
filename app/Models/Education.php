<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $fillable = ['user_id', 'subject', 'institution', 'url', 'description', 'started_at', 'ended_at'];
}
