<?php

const USER_PENDING_STATUS = 0;
const USER_ACTIVE_STATUS = 1;


const SUPER_ADMIN_ROLE = 1;
const ADMIN_ROLE = 2;
const USER_ROLE = 3;

const PENDING_STATUS = 0;
const ACTIVE_STATUS = 1;
const DELETE_STATUS = 5;

const EXPIRE_TIME_OF_FORGET_PASSWORD_CODE = 10;

const TWO = 2;
const THREE = 3;
const FOUR = 4;
const FIVE = 5;

const PAGINATE_SMALL=10;
const PAGINATE_MEDIUM=20;
const PAGINATE_LARGE=50;

const INACTIVE = 0;
const ACTIVE = 1;

const THEME_DARK = 'dark';
const THEME_LIGHT = 'light';

const FILL_RED = 'red';
const FILL_GREEN = 'green';
const FILL_BLUE = 'blue';
const FILL_BLUE_MUNSELL = 'blue_munsell';
const FILL_YELLOW = 'yellow';
const FILL_PINK = 'pink';
const FILL_DARK_GREEN = 'dark_green';
const FILL_PURPLE = 'purple';
