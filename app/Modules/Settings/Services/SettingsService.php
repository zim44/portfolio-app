<?php


namespace App\Modules\Settings\Services;

use App\Modules\Settings\Repositories\UserSettingsRepository;
use Illuminate\Support\Facades\Auth;

class SettingsService
{
    private $errorMessage;
    private $errorResponse;
    private $userSettingsRepository;

    /**
     * SettingsService constructor.
     * @param UserSettingsRepository $userSettingsRepository
     */
    public function __construct(UserSettingsRepository $userSettingsRepository)
    {
        $this->userSettingsRepository = $userSettingsRepository;
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }

    /**
     * @param $slug
     * @param $value
     * @return array
     */
    public function updateSettings($slug, $value)
    {
        try{
            $where = [
                'user_id' => Auth::user()->id,
                'slug' => decrypt($slug)
            ];
            $settingsData = ['value' => decrypt($value)];
            $this->userSettingsRepository->updateOrCreate($where,$settingsData);

            return [
                'success' => true,
                'message' => 'Settings has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Settings has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }
}
