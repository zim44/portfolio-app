<?php

Route::group(['middleware' => ['auth', 'verified', 'user']], function () {
    Route::get('messages', "MessageController@messages")->name('account.messages');
    Route::get('get-message-list', 'MessageController@getMessageList')->name('account.getMessageList');
});

