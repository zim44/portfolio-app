<?php


namespace App\Modules\Account\Messages\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\ContactMessage;
use App\User;

class ContactMessageRepository extends CommonRepository
{
    /**
     * HeaderMenuRepository constructor.
     */
    public function __construct()
    {
        $model = new ContactMessage();
        parent::__construct($model);
    }
}
