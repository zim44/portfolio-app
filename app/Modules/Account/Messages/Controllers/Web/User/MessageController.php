<?php

namespace App\Modules\Account\Messages\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Modules\Account\Messages\Services\MessageService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class MessageController extends Controller
{
    private $messageService;

    /**
     * MessageController constructor.
     * @param MessageService $messageService
     */
    public function __construct(MessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    /**
     * @return Application|Factory|View
     */
    public function messages()
    {
        $data['mainMenu'] = 'account';
        $data['submenu'] = 'messages';
        $data['user'] = Auth::user();
        $data['messages'] = $this->messageService->messages();

        return view('user.account.messages', $data);
    }

    /**
     * @return mixed
     */
    public function  getMessageList()
    {
        return $this->messageService->messageListQuery();
    }
}
