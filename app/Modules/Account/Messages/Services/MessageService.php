<?php


namespace App\Modules\Account\Messages\Services;


use App\Modules\Account\Messages\Repositories\ContactMessageRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class MessageService
{
    private $errorMessage;
    private $errorResponse;
    private $contactMessageRepository;

    /**
     * MessageService constructor.
     * @param ContactMessageRepository $contactMessageRepository
     */
    public function __construct(ContactMessageRepository $contactMessageRepository)
    {
        $this->contactMessageRepository = $contactMessageRepository;
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function messages()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->contactMessageRepository->getWhere($where);
    }

    /**
     * @return array|JsonResponse|mixed
     */
    public function messageListQuery() {
        $products = $this->contactMessageRepository->getAllQuery();
        try {
            return datatables($products)
                ->editColumn('name', function ($item) {
                    return $item->first_name . ' ' . $item->last_name;
                })
                ->editColumn('email', function ($item) {
                    return $item->email;
                })
                ->addColumn('message', function ($item) {
                    return substr($item->message, 0, 50) . '...';
                })
                ->addColumn('actions', function ($item) {
                    $generatedData = '<ul class="d-flex justify-content-center activity-menus mb-0">';

                    $generatedData .= '<a class="text-primary" href="';
                    $generatedData .= ''; //route('contact.messageDetails', encrypt($item->id));
                    $generatedData .= '" data-toggle="tooltip" title="Show">';
                    $generatedData .= '<i class="fa fa-eye"></i>';
                    $generatedData .= '</a>';
                    $generatedData .= '</ul>';

                    return $generatedData;
                })
                ->rawColumns(['actions'])
                ->make(true);
        } catch (\Exception $e) {
            return [];
        }
    }
}
