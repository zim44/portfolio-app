<?php

namespace App\Modules\Customization\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Modules\Customization\Requests\AddMenuRequest;
use App\Modules\Customization\Services\CustomizationService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class CustomizationController extends Controller
{
    private $customizationService;

    /**
     * CustomizationController constructor.
     * @param CustomizationService $customizationService
     */
    public function __construct(CustomizationService $customizationService)
    {
        $this->customizationService = $customizationService;
    }

    /**
     * @return Application|Factory|View
     */
    public function customize()
    {
        $data['mainMenu'] = 'customization';
        $data['user'] = Auth::user();

        return view('user.customization.manual', $data);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function addMenu($encryptedId)
    {
        $response = $this->customizationService->addHeaderMenu($encryptedId);

        return $response['success'] ?
            redirect()->route('customize')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deleteMenu($encryptedId)
    {
        $response = $this->customizationService->deleteHeaderMenu($encryptedId);

        return $response['success'] ?
            redirect()->route('customize')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }
}
