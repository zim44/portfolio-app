<?php


namespace App\Modules\Customization\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\HeaderMenu;

class HeaderMenuRepository extends CommonRepository
{
    /**
     * HeaderMenuRepository constructor.
     */
    public function __construct()
    {
        $model = new HeaderMenu();
        parent::__construct($model);
    }
}
