<?php


namespace App\Modules\Customization\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\UserHeaderMenu;

class UserHeaderMenuRepository extends CommonRepository
{
    /**
     * UserHeaderMenuRepository constructor.
     */
    public function __construct()
    {
        $model = new UserHeaderMenu();
        parent::__construct($model);
    }
}
