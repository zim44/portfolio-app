<?php


namespace App\Modules\Customization\Repositories;

use App\Http\Repositories\CommonRepository;
use App\User;

class UserRepository extends CommonRepository
{
    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $model = new User();
        parent::__construct($model);
    }
}
