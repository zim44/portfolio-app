<?php


namespace App\Modules\Customization\Services;

use App\Modules\Customization\Repositories\HeaderMenuRepository;
use App\Modules\Customization\Repositories\UserHeaderMenuRepository;
use App\Modules\Profile\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class CustomizationService
{
    private $errorMessage;
    private $errorResponse;
    private $userRepository;
    private $headerMenuRepository;
    private $userHeaderMenuRepository;

    /**
     * CustomizationService constructor.
     * @param UserRepository $userRepository
     * @param HeaderMenuRepository $headerMenuRepository
     * @param UserHeaderMenuRepository $userHeaderMenuRepository
     */
    public function __construct(UserRepository $userRepository, HeaderMenuRepository $headerMenuRepository, UserHeaderMenuRepository $userHeaderMenuRepository)
    {
        $this->userRepository = $userRepository;
        $this->headerMenuRepository = $headerMenuRepository;
        $this->userHeaderMenuRepository = $userHeaderMenuRepository;
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function addHeaderMenu($encryptedId)
    {
        try {
            $userHeaderMenuData = [
                'user_id' => Auth::user()->id,
                'header_menu_id' => decrypt($encryptedId),
            ];
            $this->userHeaderMenuRepository->create($userHeaderMenuData);

            return [
                'success' => true,
                'message' => 'Menu Added.',
                'webResponse' => [
                    'success' => 'Menu Added.',
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deleteHeaderMenu($encryptedId)
    {
        try{
            $where = [
                'user_id' => Auth::user()->id,
                'header_menu_id' => decrypt($encryptedId)
            ];
            $this->userHeaderMenuRepository->deleteWhere($where);

            return [
                'success' => true,
                'message' => 'Menu has been deleted.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Menu has been deleted.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }
}
