<?php

Route::group(['middleware' => ['auth', 'verified', 'user']], function () {
    Route::get('profile', "ProfileController@profile")->name('profile');
    Route::post('update-profile', "ProfileController@updateProfile")->name('updateProfile');
    Route::get('generate-sharable-link', "ProfileController@generateSharableLink")->name('generateSharableLink');
    Route::get('delete-sharable-link/{encryptedId}', "ProfileController@deleteSharableLink")->name('deleteSharableLink');
    Route::get('activate-sharable-link/{encryptedId}', "ProfileController@activateSharableLink")->name('activateSharableLink');
    Route::get('deactivate-sharable-link/{encryptedId}', "ProfileController@deactivateSharableLink")->name('deactivateSharableLink');
});

