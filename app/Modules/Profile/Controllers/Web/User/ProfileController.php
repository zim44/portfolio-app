<?php

namespace App\Modules\Profile\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Modules\Profile\Requests\UpdateProfileRequest;
use App\Modules\Profile\Services\ProfileService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ProfileController extends Controller
{
    private $profileService;

    /**
     * ProfileController constructor.
     * @param ProfileService $profileService
     */
    public function __construct(ProfileService $profileService)
    {
        $this->profileService = $profileService;
    }

    /**
     * @return Application|Factory|View
     */
    public function profile()
    {
        $data['mainMenu'] = 'customization';
        $data['submenu'] = 'profile';
        $data['user'] = Auth::user();
        $data['sharableLinks'] = $this->profileService->sharableLinks();

        return view('user.profile.details', $data);
    }

    /**
     * @param UpdateProfileRequest $request
     * @return RedirectResponse
     */
    public function updateProfile(UpdateProfileRequest $request)
    {
        $response = $this->profileService->updateProfile($request);

        return $response['success'] ?
            redirect()->route('profile')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @return RedirectResponse
     */
    public function generateSharableLink()
    {
        $response = $this->profileService->generateSharableLink();

        return $response['success'] ?
            redirect()->route('profile')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deleteSharableLink($encryptedId)
    {
        $response = $this->profileService->deleteLink($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function activateSharableLink($encryptedId)
    {
        $response = $this->profileService->activateLink($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deactivateSharableLink($encryptedId)
    {
        $response = $this->profileService->deactivateLink($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }
}
