<?php


namespace App\Modules\HeaderMenus\Skills\Services;

use App\Modules\HeaderMenus\Home\Repositories\UserProfessionRepository;
use App\Modules\HeaderMenus\Skills\Repositories\ProfessionalSkillRepository;
use App\Modules\HeaderMenus\Skills\Repositories\TechnicalSkillRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Support\Facades\Auth;

class SkillsService
{
    private $errorMessage;
    private $errorResponse;
    private $professionalSkillRepository;
    private $technicalSkillRepository;

    /**
     * SkillService constructor.
     * @param ProfessionalSkillRepository $professionalSkillRepository
     * @param TechnicalSkillRepository $technicalSkillRepository
     */
    public function __construct(ProfessionalSkillRepository $professionalSkillRepository, TechnicalSkillRepository $technicalSkillRepository)
    {
        $this->professionalSkillRepository = $professionalSkillRepository;
        $this->technicalSkillRepository = $technicalSkillRepository;
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function userTechnicalSkills()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->technicalSkillRepository->getWhere($where);
    }

    /**
     * @return mixed
     */
    public function userProfessionalSkills()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->professionalSkillRepository->getWhere($where);
    }

    /**
     * @param $request
     * @return array
     */
    public function storeTechnicalSkill($request)
    {
        try{
            $preparedData = $this->prepareTechnicalSkillData($request);
            $this->technicalSkillRepository->create($preparedData);

            return [
                'success' => true,
                'message' => 'Technical Skill has been created.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Technical Skill has been created.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateTechnicalSkill($request)
    {
        try{
            $where = ['id' => $request->id];
            $preparedData = $this->prepareTechnicalSkillData($request);
            $this->technicalSkillRepository->update($where, $preparedData);

            return [
                'success' => true,
                'message' => 'Technical Skill has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Technical Skill has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function prepareTechnicalSkillData($request)
    {
        return [
            'user_id' => Auth::user()->id,
            'title' => $request->title,
            'percentage' => $request->percentage
        ];
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deleteTechnicalSkill($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $this->technicalSkillRepository->deleteWhere($where);

            return [
                'success' => true,
                'message' => 'Technical Skill has been deleted.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Technical Skill has been deleted.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function storeProfessionalSkill($request)
    {
        try{
            $preparedData = $this->prepareProfessionalSkillData($request);
            $this->professionalSkillRepository->create($preparedData);

            return [
                'success' => true,
                'message' => 'Professional Skill has been created.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Professional Skill has been created.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateProfessionalSkill($request)
    {
        try{
            $where = ['id' => $request->id];
            $preparedData = $this->prepareProfessionalSkillData($request);
            $this->professionalSkillRepository->update($where, $preparedData);

            return [
                'success' => true,
                'message' => 'Professional Skill has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Professional Skill has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function prepareProfessionalSkillData($request)
    {
        return [
            'user_id' => Auth::user()->id,
            'title' => $request->title,
            'percentage' => $request->percentage
        ];
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deleteProfessionalSkill($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $this->professionalSkillRepository->deleteWhere($where);

            return [
                'success' => true,
                'message' => 'Professional Skill has been deleted.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Professional Skill has been deleted.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }
}
