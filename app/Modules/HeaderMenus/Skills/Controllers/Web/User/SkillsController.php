<?php

namespace App\Modules\HeaderMenus\Skills\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Modules\HeaderMenus\Skills\Requests\StoreProfessionalSkillRequest;
use App\Modules\HeaderMenus\Skills\Requests\StoreTechnicalSkillRequest;
use App\Modules\HeaderMenus\Skills\Requests\UpdateProfessionalSkillRequest;
use App\Modules\HeaderMenus\Skills\Requests\UpdateTechnicalSkillRequest;
use App\Modules\HeaderMenus\Skills\Services\SkillsService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class SkillsController extends Controller
{
    private $skillService;

    /**
     * SkillsController constructor.
     * @param SkillsService $skillService
     */
    public function __construct(SkillsService $skillService)
    {
        $this->skillService = $skillService;
    }

    /**
     * @return Application|Factory|View
     */
    public function skills()
    {
        $data['mainMenu'] = 'customization';
        $data['submenu'] = 'skills';
        $data['user'] = Auth::user();
        $data['technicalSkills'] = $this->skillService->userTechnicalSkills();
        $data['professionalSkills'] = $this->skillService->userProfessionalSkills();

        return view('user.header_menus.skills', $data);
    }

    /**
     * @param StoreTechnicalSkillRequest $request
     * @return RedirectResponse
     */
    public function storeTechnicalSkill(StoreTechnicalSkillRequest $request)
    {
        $response = $this->skillService->storeTechnicalSkill($request);

        return $response['success'] ?
            redirect()->route('headerMenu.skills')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param UpdateTechnicalSkillRequest $request
     * @return RedirectResponse
     */
    public function updateTechnicalSkill(UpdateTechnicalSkillRequest $request)
    {
        $response = $this->skillService->updateTechnicalSkill($request);

        return $response['success'] ?
            redirect()->route('headerMenu.skills')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deleteTechnicalSkill($encryptedId)
    {
        $response = $this->skillService->deleteTechnicalSkill($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param StoreProfessionalSkillRequest $request
     * @return RedirectResponse
     */
    public function storeProfessionalSkill(StoreProfessionalSkillRequest $request)
    {
        $response = $this->skillService->storeProfessionalSkill($request);

        return $response['success'] ?
            redirect()->route('headerMenu.skills')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param UpdateProfessionalSkillRequest $request
     * @return RedirectResponse
     */
    public function updateProfessionalSkill(UpdateProfessionalSkillRequest $request)
    {
        $response = $this->skillService->updateProfessionalSkill($request);

        return $response['success'] ?
            redirect()->route('headerMenu.skills')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deleteProfessionalSkill($encryptedId)
    {
        $response = $this->skillService->deleteProfessionalSkill($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }
}
