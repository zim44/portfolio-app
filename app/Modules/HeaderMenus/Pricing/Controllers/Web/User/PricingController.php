<?php

namespace App\Modules\HeaderMenus\Pricing\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Modules\HeaderMenus\Pricing\Requests\StorePriceRequest;
use App\Modules\HeaderMenus\Pricing\Requests\UpdatePriceRequest;
use App\Modules\HeaderMenus\Pricing\Services\PricingService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class PricingController extends Controller
{
    private $pricingService;

    /**
     * PricingController constructor.
     * @param PricingService $pricingService
     */
    public function __construct(PricingService $pricingService)
    {
        $this->pricingService = $pricingService;
    }

    /**
     * @return Application|Factory|View
     */
    public function pricing()
    {
        $data['mainMenu'] = 'customization';
        $data['submenu'] = 'pricing';
        $data['user'] = Auth::user();
        $data['prices'] = $this->pricingService->prices();

        return view('user.header_menus.pricing', $data);
    }

    /**
     * @param StorePriceRequest $request
     * @return RedirectResponse
     */
    public function storePrice(StorePriceRequest $request)
    {
        $response = $this->pricingService->storePrice($request);

        return $response['success'] ?
            redirect()->route('headerMenu.pricing')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param UpdatePriceRequest $request
     * @return RedirectResponse
     */
    public function updatePrice(UpdatePriceRequest $request)
    {
        $response = $this->pricingService->updatePrice($request);

        return $response['success'] ?
            redirect()->route('headerMenu.pricing')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deletePrice($encryptedId)
    {
        $response = $this->pricingService->deletePrice($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function activatePrice($encryptedId)
    {
        $response = $this->pricingService->activatePrice($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deactivatePrice($encryptedId)
    {
        $response = $this->pricingService->deactivatePrice($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }
}
