<?php

namespace App\Modules\HeaderMenus\Pricing\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePriceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'subtitle' => 'required|string',
            'url' => 'required|string',
            'price' => 'required|numeric|min:0',
            'content' => 'required|string',
            'status' => 'required|between:0,1',
        ];
    }
}
