<?php


namespace App\Modules\HeaderMenus\Pricing\Services;

use App\Modules\HeaderMenus\Pricing\Repositories\PriceRepository;
use Illuminate\Support\Facades\Auth;

class PricingService
{
    private $errorMessage;
    private $errorResponse;
    private $priceRepository;

    /**
     * PricingService constructor.
     * @param PriceRepository $priceRepository
     */
    public function __construct(PriceRepository $priceRepository)
    {
        $this->priceRepository = $priceRepository;
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function prices()
    {
        $where = ['user_id' => Auth::user()->id];
        $prices = $this->priceRepository->getWhere($where);
        foreach ($prices as $price){
            $price['contents'] = explode(',', $price->content);
        }

        return $prices;
    }

    /**
     * @param $request
     * @return array
     */
    public function storePrice($request)
    {
        try{
            $preparedData = $this->preparePriceData($request);
            $this->priceRepository->create($preparedData);

            return [
                'success' => true,
                'message' => 'Price has been created.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Price has been created.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updatePrice($request)
    {
        try{
            $where = ['id' => $request->id];
            $preparedData = $this->preparePriceData($request);
            $this->priceRepository->update($where, $preparedData);

            return [
                'success' => true,
                'message' => 'Price has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Price has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function preparePriceData($request)
    {
        return [
            'user_id' => Auth::user()->id,
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'url' => $request->url,
            'amount' => $request->price,
            'content' => $request->content,
            'status' => $request->status
        ];
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deletePrice($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $this->priceRepository->deleteWhere($where);

            return [
                'success' => true,
                'message' => 'Price has been deleted.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Price has been deleted.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function activatePrice($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $data = ['status' => ACTIVE];
            $this->priceRepository->update($where, $data);

            return [
                'success' => true,
                'message' => 'Price has been activated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Price has been activated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deactivatePrice($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $data = ['status' => INACTIVE];
            $this->priceRepository->update($where, $data);

            return [
                'success' => true,
                'message' => 'Price has been deactivated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Price has been deactivated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }
}
