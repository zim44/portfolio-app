<?php

Route::group(['middleware' => ['auth', 'verified', 'user']], function () {
    Route::get('experiences', "ExperiencesController@experiences")->name('headerMenu.experiences');
    Route::post('add-education', "ExperiencesController@storeEducation")->name('headerMenu.storeEducation');
    Route::post('edit-education', "ExperiencesController@updateEducation")->name('headerMenu.updateEducation');
    Route::get('delete-education/{encryptedId}', "ExperiencesController@deleteEducation")->name('headerMenu.deleteEducation');
    Route::post('add-work-experience', "ExperiencesController@storeWorkExperience")->name('headerMenu.storeWorkExperience');
    Route::post('edit-work-experience', "ExperiencesController@updateWorkExperience")->name('headerMenu.updateWorkExperience');
    Route::get('delete-work-experience/{encryptedId}', "ExperiencesController@deleteWorkExperience")->name('headerMenu.deleteWorkExperience');
});

