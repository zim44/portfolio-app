<?php

namespace App\Modules\HeaderMenus\Experiences\Requests;

use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class UpdateWorkExperienceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric',
            'job_position' => 'required|string',
            'company' => 'required|string',
            'url' => 'required|string',
            'responsibility' => 'required|string',
            'started_at' => 'required|date|before:'.Carbon::now(),
            'ended_at' => isset($this->ended_at) ? 'date|after:started_at|before:'.Carbon::now() : '',
        ];
    }
}
