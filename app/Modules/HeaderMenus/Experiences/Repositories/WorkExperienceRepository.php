<?php


namespace App\Modules\HeaderMenus\Experiences\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\WorkExperience;

class WorkExperienceRepository extends CommonRepository
{
    public function __construct()
    {
        $model = new WorkExperience();
        parent::__construct($model);
    }
}
