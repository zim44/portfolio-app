<?php

namespace App\Modules\HeaderMenus\Blog\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string',
            'site' => 'required|string',
            'url' => 'required|string',
            'content' => 'required|string',
            'image' => 'required|mimes:jpeg,jpg,JPG,png,PNG,gif|max:4000',
        ];
    }
}
