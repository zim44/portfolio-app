<?php


namespace App\Modules\HeaderMenus\Blog\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\Post;

class PostRepository extends CommonRepository
{
    /**
     * BlogRepository constructor.
     */
    public function __construct()
    {
        $model = new Post();
        parent::__construct($model);
    }
}
