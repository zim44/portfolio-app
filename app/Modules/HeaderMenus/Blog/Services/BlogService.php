<?php


namespace App\Modules\HeaderMenus\Blog\Services;

use App\Modules\HeaderMenus\Blog\Repositories\PostRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class BlogService
{
    private $errorMessage;
    private $errorResponse;
    private $postRepository;

    /**
     * BlogService constructor.
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function posts()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->postRepository->getWhere($where);
    }

    /**
     * @param $request
     * @return array
     */
    public function storePost($request)
    {
        try{
            $preparedData = $this->preparePostData($request);
            $this->postRepository->create($preparedData);

            return [
                'success' => true,
                'message' => 'Post has been created.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Post has been created.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function preparePostData($request)
    {
        return [
            'user_id' => Auth::user()->id,
            'title' => $request->title,
            'site' => $request->site,
            'url' => $request->url,
            'content' => $request->content,
            'image' => uploadFile($request->image, blogImagePath())
        ];
    }

    /**
     * @param $request
     * @return array
     */
    public function updatePost($request)
    {
        try{
            $where = ['id' => $request->id];
            $preparedData = $this->prepareUpdatedPostData($request);
            $this->postRepository->update($where, $preparedData);

            return [
                'success' => true,
                'message' => 'Post has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Post has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function prepareUpdatedPostData($request)
    {
        $preparedData = [
            'user_id' => Auth::user()->id,
            'title' => $request->title,
            'site' => $request->site,
            'url' => $request->url,
            'content' => $request->content
        ];
        if(isset($request['image'])){
            $where = ['id' => $request->id];
            $oldImage = $this->postRepository->pluckWhere($where, 'image');
            $preparedData['image'] = uploadFile($request->image, blogImagePath(), $oldImage);
        }

        return $preparedData;
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deletePost($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $this->postRepository->deleteWhere($where);

            return [
                'success' => true,
                'message' => 'Post has been deleted.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Post has been deleted.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }
}
