<?php

namespace App\Modules\HeaderMenus\Portfolio\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Modules\HeaderMenus\Portfolio\Requests\StorePortfolioCategoryRequest;
use App\Modules\HeaderMenus\Portfolio\Requests\StorePortfolioContentRequest;
use App\Modules\HeaderMenus\Portfolio\Requests\UpdatePortfolioCategoryRequest;
use App\Modules\HeaderMenus\Portfolio\Requests\UpdatePortfolioContentRequest;
use App\Modules\HeaderMenus\Portfolio\Services\PortfolioService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class PortfolioController extends Controller
{
    private $portfolioService;

    /**
     * PortfolioController constructor.
     * @param PortfolioService $portfolioService
     */
    public function __construct(PortfolioService $portfolioService)
    {
        $this->portfolioService = $portfolioService;
    }

    /**
     * @return Application|Factory|View
     */
    public function portfolio()
    {
        $data['mainMenu'] = 'customization';
        $data['submenu'] = 'portfolio';
        $data['user'] = Auth::user();
        $data['portfolioCategories'] = $this->portfolioService->portfolioCategories();
        $data['portfolioContents'] = $this->portfolioService->portfolioContents();

        return view('user.header_menus.portfolio', $data);
    }

    /**
     * @param StorePortfolioCategoryRequest $request
     * @return RedirectResponse
     */
    public function storePortfolioCategory(StorePortfolioCategoryRequest $request)
    {
        $response = $this->portfolioService->storePortfolioCategory($request);

        return $response['success'] ?
            redirect()->route('headerMenu.portfolio')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }
    public function updatePortfolioCategory(UpdatePortfolioCategoryRequest $request)
    {
        $response = $this->portfolioService->updatePortfolioCategory($request);

        return $response['success'] ?
            redirect()->route('headerMenu.portfolio')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deletePortfolioCategory($encryptedId)
    {
        $response = $this->portfolioService->deletePortfolioCategory($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param StorePortfolioContentRequest $request
     * @return RedirectResponse
     */
    public function storePortfolioContent(StorePortfolioContentRequest $request)
    {
        $response = $this->portfolioService->storePortfolioContent($request);

        return $response['success'] ?
            redirect()->route('headerMenu.portfolio')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param UpdatePortfolioContentRequest $request
     * @return RedirectResponse
     */
    public function updatePortfolioContent(UpdatePortfolioContentRequest $request)
    {
        $response = $this->portfolioService->updatePortfolioContent($request);

        return $response['success'] ?
            redirect()->route('headerMenu.portfolio')->with($response['webResponse']) :
            redirect()->back()->with($response['webResponse']);
    }

    /**
     * @param $encryptedId
     * @return RedirectResponse
     */
    public function deletePortfolioContent($encryptedId)
    {
        $response = $this->portfolioService->deletePortfolioContent($encryptedId);

        return redirect()->back()->with($response['webResponse']);
    }
}
