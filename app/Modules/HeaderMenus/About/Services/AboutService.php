<?php


namespace App\Modules\HeaderMenus\About\Services;

use App\Modules\HeaderMenus\About\Repositories\AboutRepository;
use App\Modules\HeaderMenus\About\Repositories\TechnicalSkillRepository;
use App\Modules\HeaderMenus\About\Repositories\UserActivityRepository;
use App\Modules\HeaderMenus\About\Repositories\UserFeaturedProjectCommentRepository;
use App\Modules\HeaderMenus\About\Repositories\UserFeaturedProjectRepository;
use Illuminate\Support\Facades\Auth;

class AboutService
{
    private $errorMessage;
    private $errorResponse;
    private $technicalSkillRepository;
    private $aboutRepository;
    private $userActivityRepository;
    private $userFeaturedProjectRepository;
    private $userFeaturedProjectCommentRepository;

    /**
     * AboutService constructor.
     * @param TechnicalSkillRepository $technicalSkillRepository
     * @param AboutRepository $aboutRepository
     * @param UserActivityRepository $userActivityRepository
     * @param UserFeaturedProjectRepository $userFeaturedProjectRepository
     * @param UserFeaturedProjectCommentRepository $userFeaturedProjectCommentRepository
     */
    public function __construct(
        TechnicalSkillRepository $technicalSkillRepository,
        AboutRepository $aboutRepository,
        UserActivityRepository $userActivityRepository,
        UserFeaturedProjectRepository $userFeaturedProjectRepository,
        UserFeaturedProjectCommentRepository $userFeaturedProjectCommentRepository
    )
    {
        $this->technicalSkillRepository = $technicalSkillRepository;
        $this->aboutRepository = $aboutRepository;
        $this->userActivityRepository = $userActivityRepository;
        $this->userFeaturedProjectRepository = $userFeaturedProjectRepository;
        $this->userFeaturedProjectCommentRepository = $userFeaturedProjectCommentRepository;
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function userAbout()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->aboutRepository->whereLast($where);
    }

    /**
     * @return mixed
     */
    public function userTechnicalSkills()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->technicalSkillRepository->getWhere($where);
    }

    /**
     * @return mixed
     */
    public function userActivities()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->userActivityRepository->getWhere($where);
    }

    /**
     * @return mixed
     */
    public function userFeaturedProjects()
    {
        $where = ['user_id' => Auth::user()->id];
        $projects = $this->userFeaturedProjectRepository->getWhere($where);
        foreach ($projects as $project){
            $where = ['user_featured_project_id' => $project->id];
            $project['comments'] = $this->userFeaturedProjectCommentRepository->getWhere($where);
        }

        return $projects;
    }

    /**
     * @param $request
     * @return array
     */
    public function updateDescription($request)
    {
        try{
            $where = ['user_id' => Auth::user()->id];
            $preparedData = $this->prepareUpdatedAboutData($request);
            $this->aboutRepository->updateOrCreate($where, $preparedData);

            return [
                'success' => true,
                'message' => 'Description has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Description has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function prepareUpdatedAboutData($request)
    {
        $preparedData = ['description' => $request->description];
        if(isset($request['image'])){
            $where = ['user_id' => Auth::user()->id];
            $oldImage = $this->aboutRepository->pluckWhere($where, 'image');
            $preparedData['image'] = uploadFile($request->image, aboutImagePath(), $oldImage);
        }

        return $preparedData;
    }

    /**
     * @param $request
     * @return array
     */
    public function storeActivity($request)
    {
        try{
            $preparedData = $this->prepareActivityData($request);
            $this->userActivityRepository->create($preparedData);

            return [
                'success' => true,
                'message' => 'Activity has been created.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Activity has been created.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateActivity($request)
    {
        try{
            $where = ['id' => $request->id];
            $preparedData = $this->prepareActivityData($request);
            $this->userActivityRepository->update($where, $preparedData);

            return [
                'success' => true,
                'message' => 'Activity has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Activity has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function prepareActivityData($request)
    {
        return [
            'user_id' => Auth::user()->id,
            'logo' => 'logo',
            'title' => $request->title,
            'description' => $request->description
        ];
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deleteActivity($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $this->userActivityRepository->deleteWhere($where);

            return [
                'success' => true,
                'message' => 'Activity has been deleted.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Activity has been deleted.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function storeFeaturedProject($request)
    {
        try{
            $preparedData = $this->prepareFeaturedProjectData($request);
            $this->userFeaturedProjectRepository->create($preparedData);

            return [
                'success' => true,
                'message' => 'Featured Project has been created.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Featured Project has been created.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function prepareFeaturedProjectData($request)
    {
        $preparedData = [
            'user_id' => Auth::user()->id,
            'project_type' => $request->project_type,
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'description' => $request->description,
            'image' => uploadFile($request->image, projectImagePath())
        ];
        if(isset($request['url'])){
            $preparedData['url'] = $request->url;
        }

        return $preparedData;
    }

    /**
     * @param $request
     * @return array
     */
    public function updateFeaturedProject($request)
    {
        try{
            $where = ['id' => $request->id];
            $preparedData = $this->prepareUpdatedFeaturedProjectData($request);
            $this->userFeaturedProjectRepository->update($where, $preparedData);

            return [
                'success' => true,
                'message' => 'Featured Project has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Featured Project has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function prepareUpdatedFeaturedProjectData($request)
    {
        $preparedData = [
            'user_id' => Auth::user()->id,
            'project_type' => $request->project_type,
            'title' => $request->title,
            'subtitle' => $request->subtitle,
            'description' => $request->description,
        ];
        if(isset($request['image'])){
            $where = ['id' => $request->id];
            $oldImage = $this->userFeaturedProjectRepository->pluckWhere($where, 'image');
            $preparedData['image'] = uploadFile($request->image, projectImagePath(), $oldImage);
        }
        if(isset($request['url'])){
            $preparedData['url'] = $request->url;
        }

        return $preparedData;
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deleteFeaturedProject($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $this->userFeaturedProjectRepository->deleteWhere($where);

            return [
                'success' => true,
                'message' => 'Featured Project has been deleted.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Featured Project has been deleted.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function storeProjectComment($request)
    {
        try{
            $preparedData = $this->prepareProjectCommentData($request);
            $this->userFeaturedProjectCommentRepository->create($preparedData);

            return [
                'success' => true,
                'message' => 'Project Comment has been created.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Project Comment has been created.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    public function updateProjectComment($request)
    {
        try{
            $where = [
                'id' => $request->id,
                'user_featured_project_id' => $request->user_featured_project_id
            ];
            $preparedData = $this->prepareProjectCommentData($request);
            $this->userFeaturedProjectCommentRepository->update($where, $preparedData);

            return [
                'success' => true,
                'message' => 'Project Comment has been updated.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Project Comment has been updated.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function prepareProjectCommentData($request)
    {
        return [
            'user_featured_project_id' => $request->user_featured_project_id,
            'person' => $request->person,
            'comment' => $request->comment,
        ];
    }

    /**
     * @param $encryptedId
     * @return array
     */
    public function deleteProjectComment($encryptedId)
    {
        try{
            $where = ['id' => decrypt($encryptedId)];
            $this->userFeaturedProjectCommentRepository->deleteWhere($where);

            return [
                'success' => true,
                'message' => 'Project Comment has been deleted.',
                'data' => [],
                'webResponse' => [
                    'success' => 'Project Comment has been deleted.'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }
}
