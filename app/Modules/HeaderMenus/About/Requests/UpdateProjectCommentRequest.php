<?php

namespace App\Modules\HeaderMenus\About\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProjectCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric',
            'user_featured_project_id' => 'required|numeric',
            'person' => 'required|string',
            'comment' => 'required|string',
        ];
    }
}
