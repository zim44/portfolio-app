<?php


namespace App\Modules\HeaderMenus\About\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\About;

class AboutRepository extends CommonRepository
{
    /**
     * AboutRepository constructor.
     */
    public function __construct()
    {
        $model = new About();
        parent::__construct($model);
    }
}
