<?php


namespace App\Modules\HeaderMenus\About\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\UserActivity;

class UserActivityRepository extends CommonRepository
{
    /**
     * UserActivityRepository constructor.
     */
    public function __construct()
    {
        $model = new UserActivity();
        parent::__construct($model);
    }
}
