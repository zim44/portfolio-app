<?php


namespace App\Modules\HeaderMenus\About\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\UserFeaturedProjectComment;

class UserFeaturedProjectCommentRepository extends CommonRepository
{
    /**
     * UserFeaturedProjectRepository constructor.
     */
    public function __construct()
    {
        $model = new UserFeaturedProjectComment();
        parent::__construct($model);
    }
}
