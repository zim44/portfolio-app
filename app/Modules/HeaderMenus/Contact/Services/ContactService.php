<?php


namespace App\Modules\HeaderMenus\Contact\Services;


class ContactService
{
    private $errorMessage;
    private $errorResponse;

    /**
     * HomeService constructor.
     */
    public function __construct()
    {
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }
}
