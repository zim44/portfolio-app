<?php

Route::group(['middleware' => ['auth', 'verified', 'user']], function () {
    Route::get('home', "HomeController@home")->name('headerMenu.home');
    Route::post('update-profession', "HomeController@updateProfession")->name('headerMenu.updateProfession');
});

