<?php


namespace App\Modules\HeaderMenus\Home\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\UserProfession;

class UserProfessionRepository extends CommonRepository
{
    /**
     * HeaderMenuRepository constructor.
     */
    public function __construct()
    {
        $model = new UserProfession();
        parent::__construct($model);
    }
}
