<?php

namespace App\Modules\Preview\Services;

use App\Modules\Preview\Repositories\EducationRepository;
use App\Modules\Preview\Repositories\WorkExperienceRepository;
use App\Modules\Preview\Repositories\AboutRepository;
use App\Modules\Preview\Repositories\PortfolioCategoryRepository;
use App\Modules\Preview\Repositories\PortfolioContentRepository;
use App\Modules\Preview\Repositories\PostRepository;
use App\Modules\Preview\Repositories\PriceRepository;
use App\Modules\Preview\Repositories\ProfessionalSkillRepository;
use App\Modules\Preview\Repositories\TechnicalSkillRepository;
use App\Modules\Preview\Repositories\UserActivityRepository;
use App\Modules\Preview\Repositories\UserFeaturedProjectCommentRepository;
use App\Modules\Preview\Repositories\UserFeaturedProjectRepository;
use App\Modules\Preview\Repositories\UserProfessionRepository;
use App\Modules\Preview\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class PreviewService
{
    private $errorMessage;
    private $errorResponse;
    private $userRepository;
    private $userProfessionRepository;
    private $professionalSkillRepository;
    private $technicalSkillRepository;
    private $aboutRepository;
    private $userActivityRepository;
    private $userFeaturedProjectRepository;
    private $userFeaturedProjectCommentRepository;
    private $portfolioCategoryRepository;
    private $educationRepository;
    private $workExperienceRepository;
    private $portfolioContentRepository;
    private $postRepository;
    private $priceRepository;

    /**
     * PreviewService constructor.
     * @param UserRepository $userRepository
     * @param UserProfessionRepository $userProfessionRepository
     * @param ProfessionalSkillRepository $professionalSkillRepository
     * @param TechnicalSkillRepository $technicalSkillRepository
     * @param AboutRepository $aboutRepository
     * @param UserActivityRepository $userActivityRepository
     * @param UserFeaturedProjectRepository $userFeaturedProjectRepository
     * @param UserFeaturedProjectCommentRepository $userFeaturedProjectCommentRepository
     * @param EducationRepository $educationRepository
     * @param WorkExperienceRepository $workExperienceRepository
     * @param PortfolioCategoryRepository $portfolioCategoryRepository
     * @param PortfolioContentRepository $portfolioContentRepository
     * @param PostRepository $postRepository
     * @param PriceRepository $priceRepository
     */
    public function __construct(
        UserRepository $userRepository,
        UserProfessionRepository $userProfessionRepository,
        ProfessionalSkillRepository $professionalSkillRepository,
        TechnicalSkillRepository $technicalSkillRepository,
        AboutRepository $aboutRepository,
        UserActivityRepository $userActivityRepository,
        UserFeaturedProjectRepository $userFeaturedProjectRepository,
        UserFeaturedProjectCommentRepository $userFeaturedProjectCommentRepository,
        EducationRepository $educationRepository,
        WorkExperienceRepository $workExperienceRepository,
        PortfolioCategoryRepository $portfolioCategoryRepository,
        PortfolioContentRepository $portfolioContentRepository,
        PostRepository $postRepository,
        PriceRepository $priceRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->userProfessionRepository = $userProfessionRepository;
        $this->professionalSkillRepository = $professionalSkillRepository;
        $this->technicalSkillRepository = $technicalSkillRepository;
        $this->aboutRepository = $aboutRepository;
        $this->userActivityRepository = $userActivityRepository;
        $this->userFeaturedProjectRepository = $userFeaturedProjectRepository;
        $this->userFeaturedProjectCommentRepository = $userFeaturedProjectCommentRepository;
        $this->educationRepository = $educationRepository;
        $this->workExperienceRepository = $workExperienceRepository;
        $this->portfolioCategoryRepository = $portfolioCategoryRepository;
        $this->portfolioContentRepository = $portfolioContentRepository;
        $this->postRepository = $postRepository;
        $this->priceRepository = $priceRepository;
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function userProfession()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->userProfessionRepository->whereLast($where);
    }

    /**
     * @return mixed
     */
    public function userTechnicalSkills()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->technicalSkillRepository->getWhere($where);
    }

    /**
     * @return mixed
     */
    public function userProfessionalSkills()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->professionalSkillRepository->getWhere($where);
    }

    /**
     * @return mixed
     */
    public function userAbout()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->aboutRepository->whereLast($where);
    }

    /**
     * @return mixed
     */
    public function userActivities()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->userActivityRepository->getWhere($where);
    }

    /**
     * @return mixed
     */
    public function userFeaturedProjects()
    {
        $where = ['user_id' => Auth::user()->id];
        $projects = $this->userFeaturedProjectRepository->getWhere($where);
        foreach ($projects as $project){
            $where = ['user_featured_project_id' => $project->id];
            $project['comments'] = $this->userFeaturedProjectCommentRepository->getWhere($where);
        }

        return $projects;
    }

    /**
     * @return mixed
     */
    public function educations()
    {
        $where = ['user_id' => Auth::user()->id];
        $educations = $this->educationRepository->getWhere($where);
        foreach ($educations as $education){
            $education['year_started'] = explode('-', $education->started_at)[0];
            $education['year_ended'] = explode('-', $education->ended_at)[0];
        }

        return $educations;
    }

    /**
     * @return mixed
     */
    public function workExperiences()
    {
        $where = ['user_id' => Auth::user()->id];
        $workExperiences = $this->workExperienceRepository->getWhere($where);
        foreach ($workExperiences as $workExperience){
            $workExperience['responsibilities'] = explode(',', $workExperience->responsibility);
            $workExperience['year_started'] = explode('-', $workExperience->started_at)[0];
            $workExperience['year_ended'] = explode('-', $workExperience->ended_at)[0];
        }

        return $workExperiences;
    }

    /**
     * @return mixed
     */
    public function portfolioCategories()
    {
        $where = ['user_id' => Auth::user()->id];
        $portfolioCategories = $this->portfolioCategoryRepository->getWhere($where);
        foreach ($portfolioCategories as $portfolioCategory){
            $portfolioCategory['filter_key'] = preg_replace('/\s+/', '-', strtolower($portfolioCategory->title));
        }

        return $portfolioCategories;
    }

    /**
     * @return mixed
     */
    public function portfolioContents()
    {
        $where = ['user_id' => Auth::user()->id];
        $portfolioCategoryIds = $this->portfolioCategoryRepository->pluckWhere($where, 'id');
        $portfolioContents = $this->portfolioContentRepository->whereIn('portfolio_category_id', $portfolioCategoryIds);
        foreach ($portfolioContents as $portfolioContent) {
            $where = ['id' => $portfolioContent->portfolio_category_id];
            $portfolioCategory = $this->portfolioCategoryRepository->whereLast($where);
            $portfolioContent['class'] = preg_replace('/\s+/', '-', strtolower($portfolioCategory->title));
        }

        return $portfolioContents;
    }

    /**
     * @return mixed
     */
    public function prices()
    {
        $where = ['user_id' => Auth::user()->id];
        $prices = $this->priceRepository->getWhere($where);
        foreach ($prices as $price){
            $price['contents'] = explode(',', $price->content);
        }

        return $prices;
    }

    /**
     * @return mixed
     */
    public function posts()
    {
        $where = ['user_id' => Auth::user()->id];

        return $this->postRepository->getWhere($where);
    }
}
