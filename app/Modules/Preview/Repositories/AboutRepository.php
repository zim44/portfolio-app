<?php


namespace App\Modules\Preview\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\About;

class AboutRepository extends CommonRepository
{
    /**
     * TechnicalSkillRepository constructor.
     */
    public function __construct()
    {
        $model = new About();
        parent::__construct($model);
    }
}
