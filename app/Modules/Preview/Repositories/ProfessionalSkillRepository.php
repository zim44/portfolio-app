<?php


namespace App\Modules\Preview\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\ProfessionalSkill;

class ProfessionalSkillRepository extends CommonRepository
{
    /**
     * ProfessionalSkillRepository constructor.
     */
    public function __construct()
    {
        $model = new ProfessionalSkill();
        parent::__construct($model);
    }
}
