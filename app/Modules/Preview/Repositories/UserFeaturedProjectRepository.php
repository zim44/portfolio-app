<?php


namespace App\Modules\Preview\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\UserFeaturedProject;

class UserFeaturedProjectRepository extends CommonRepository
{
    /**
     * UserFeaturedProjectRepository constructor.
     */
    public function __construct()
    {
        $model = new UserFeaturedProject();
        parent::__construct($model);
    }
}
