<?php


namespace App\Modules\Preview\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\Post;

class PostRepository extends CommonRepository
{
    /**
     * BlogRepository constructor.
     */
    public function __construct()
    {
        $model = new Post();
        parent::__construct($model);
    }
}
