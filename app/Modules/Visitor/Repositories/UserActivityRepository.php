<?php


namespace App\Modules\Visitor\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\UserActivity;

class UserActivityRepository extends CommonRepository
{
    /**
     * TechnicalSkillRepository constructor.
     */
    public function __construct()
    {
        $model = new UserActivity();
        parent::__construct($model);
    }
}
