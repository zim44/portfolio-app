<?php


namespace App\Modules\Visitor\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\UserSettings;

class UserSettingsRepository extends CommonRepository
{
    /**
     * UserSettingsRepository constructor.
     */
    public function __construct()
    {
        $model = new UserSettings();
        parent::__construct($model);
    }
}
