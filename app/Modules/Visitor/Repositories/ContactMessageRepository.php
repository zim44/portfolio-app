<?php


namespace App\Modules\Visitor\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\ContactMessage;

class ContactMessageRepository extends CommonRepository
{
    /**
     * ContactMessageRepository constructor.
     */
    public function __construct()
    {
        $model = new ContactMessage();
        parent::__construct($model);
    }
}
