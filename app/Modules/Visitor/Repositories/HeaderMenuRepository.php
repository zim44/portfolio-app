<?php


namespace App\Modules\Visitor\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\HeaderMenu;

class HeaderMenuRepository extends CommonRepository
{
    /**Repository constructor.
     */
    public function __construct()
    {
        $model = new HeaderMenu();
        parent::__construct($model);
    }
}
