<?php


namespace App\Modules\Visitor\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\PortfolioCategory;

class PortfolioCategoryRepository extends CommonRepository
{
    /**
     * PortfolioCategoryRepository constructor.
     */
    public function __construct()
    {
        $model = new PortfolioCategory();
        parent::__construct($model);
    }
}
