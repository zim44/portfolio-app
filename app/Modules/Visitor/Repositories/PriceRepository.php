<?php


namespace App\Modules\Visitor\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\Price;

class PriceRepository extends CommonRepository
{
    /**
     * PriceRepository constructor.
     */
    public function __construct()
    {
        $model = new Price();
        parent::__construct($model);
    }
}
