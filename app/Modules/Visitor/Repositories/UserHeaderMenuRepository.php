<?php


namespace App\Modules\Visitor\Repositories;

use App\Http\Repositories\CommonRepository;
use App\Models\UserHeaderMenu;

class UserHeaderMenuRepository extends CommonRepository
{
    /**
     * HeaderMenuRepository constructor.
     */
    public function __construct()
    {
        $model = new UserHeaderMenu();
        parent::__construct($model);
    }
}
