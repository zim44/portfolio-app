<?php

namespace App\Modules\Visitor\Services;

use App\Modules\Visitor\Repositories\UserSettingsRepository;
use App\Modules\Visitor\Repositories\ContactMessageRepository;
use App\Modules\Visitor\Repositories\EducationRepository;
use App\Modules\Visitor\Repositories\HeaderMenuRepository;
use App\Modules\Visitor\Repositories\SharableLinkAccessKeyRepository;
use App\Modules\Visitor\Repositories\UserHeaderMenuRepository;
use App\Modules\Visitor\Repositories\WorkExperienceRepository;
use App\Modules\Visitor\Repositories\AboutRepository;
use App\Modules\Visitor\Repositories\PortfolioCategoryRepository;
use App\Modules\Visitor\Repositories\PortfolioContentRepository;
use App\Modules\Visitor\Repositories\PostRepository;
use App\Modules\Visitor\Repositories\PriceRepository;
use App\Modules\Visitor\Repositories\ProfessionalSkillRepository;
use App\Modules\Visitor\Repositories\TechnicalSkillRepository;
use App\Modules\Visitor\Repositories\UserActivityRepository;
use App\Modules\Visitor\Repositories\UserFeaturedProjectCommentRepository;
use App\Modules\Visitor\Repositories\UserFeaturedProjectRepository;
use App\Modules\Visitor\Repositories\UserProfessionRepository;
use App\Modules\Visitor\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class VisitorService
{
    private $errorMessage;
    private $errorResponse;
    private $userRepository;
    private $userSettingsRepository;
    private $headerMenuRepository;
    private $userHeaderMenuRepository;
    private $userProfessionRepository;
    private $professionalSkillRepository;
    private $technicalSkillRepository;
    private $aboutRepository;
    private $userActivityRepository;
    private $userFeaturedProjectRepository;
    private $userFeaturedProjectCommentRepository;
    private $portfolioCategoryRepository;
    private $educationRepository;
    private $workExperienceRepository;
    private $portfolioContentRepository;
    private $postRepository;
    private $priceRepository;
    private $sharableLinkAccessKeyRepository;
    private $contactMessageRepository;

    /**
     * PreviewService constructor.
     * @param UserRepository $userRepository
     * @param UserSettingsRepository $userSettingsRepository
     * @param UserProfessionRepository $userProfessionRepository
     * @param HeaderMenuRepository $headerMenuRepository
     * @param UserHeaderMenuRepository $userHeaderMenuRepository
     * @param ProfessionalSkillRepository $professionalSkillRepository
     * @param TechnicalSkillRepository $technicalSkillRepository
     * @param AboutRepository $aboutRepository
     * @param UserActivityRepository $userActivityRepository
     * @param UserFeaturedProjectRepository $userFeaturedProjectRepository
     * @param UserFeaturedProjectCommentRepository $userFeaturedProjectCommentRepository
     * @param EducationRepository $educationRepository
     * @param WorkExperienceRepository $workExperienceRepository
     * @param PortfolioCategoryRepository $portfolioCategoryRepository
     * @param PortfolioContentRepository $portfolioContentRepository
     * @param PostRepository $postRepository
     * @param PriceRepository $priceRepository
     * @param SharableLinkAccessKeyRepository $sharableLinkAccessKeyRepository
     * @param ContactMessageRepository $contactMessageRepository
     */
    public function __construct(
        UserRepository $userRepository,
        UserSettingsRepository $userSettingsRepository,
        UserProfessionRepository $userProfessionRepository,
        HeaderMenuRepository $headerMenuRepository,
        UserHeaderMenuRepository $userHeaderMenuRepository,
        ProfessionalSkillRepository $professionalSkillRepository,
        TechnicalSkillRepository $technicalSkillRepository,
        AboutRepository $aboutRepository,
        UserActivityRepository $userActivityRepository,
        UserFeaturedProjectRepository $userFeaturedProjectRepository,
        UserFeaturedProjectCommentRepository $userFeaturedProjectCommentRepository,
        EducationRepository $educationRepository,
        WorkExperienceRepository $workExperienceRepository,
        PortfolioCategoryRepository $portfolioCategoryRepository,
        PortfolioContentRepository $portfolioContentRepository,
        PostRepository $postRepository,
        PriceRepository $priceRepository,
        SharableLinkAccessKeyRepository $sharableLinkAccessKeyRepository,
        ContactMessageRepository $contactMessageRepository
    )
    {
        $this->userRepository = $userRepository;
        $this->userSettingsRepository = $userSettingsRepository;
        $this->headerMenuRepository = $headerMenuRepository;
        $this->userHeaderMenuRepository = $userHeaderMenuRepository;
        $this->userProfessionRepository = $userProfessionRepository;
        $this->professionalSkillRepository = $professionalSkillRepository;
        $this->technicalSkillRepository = $technicalSkillRepository;
        $this->aboutRepository = $aboutRepository;
        $this->userActivityRepository = $userActivityRepository;
        $this->userFeaturedProjectRepository = $userFeaturedProjectRepository;
        $this->userFeaturedProjectCommentRepository = $userFeaturedProjectCommentRepository;
        $this->educationRepository = $educationRepository;
        $this->workExperienceRepository = $workExperienceRepository;
        $this->portfolioCategoryRepository = $portfolioCategoryRepository;
        $this->portfolioContentRepository = $portfolioContentRepository;
        $this->postRepository = $postRepository;
        $this->priceRepository = $priceRepository;
        $this->sharableLinkAccessKeyRepository = $sharableLinkAccessKeyRepository;
        $this->contactMessageRepository = $contactMessageRepository;
        $this->errorMessage = __('Something went wrong');
        $this->errorResponse = [
            'success' => false,
            'message' => $this->errorMessage,
            'data' => [],
            'webResponse' => [
                'dismiss' => $this->errorMessage,
            ],
        ];
    }

    /**
     * @param $request
     * @return array
     */
    public function person($request)
    {
        $where = ['access_key' => $request->access_key];
        $linkData = $this->sharableLinkAccessKeyRepository->whereLast($where);
        if(empty($linkData)){
            $this->errorResponse['message'] = __('Oops! Portfolio not found.');
            $this->errorResponse['webResponse']['dismiss'] = __('Oops! Portfolio not found.');

            return $this->errorResponse;
        }else{
            $where = ['id' => $linkData['user_id']];
            $person = $this->userRepository->whereLast($where);

            return [
                'success' => true,
                'message' => __('Portfolio Found!'),
                'data' => $person,
                'webResponse' => [
                    'success' => __('Portfolio Found!'),
                ],
            ];
        }
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function settings($personId)
    {
        $settings =[];
        $where = [
            'user_id' => $personId,
            'slug' => 'theme'
        ];
        $theme = $this->userSettingsRepository->whereLast($where);
        $where = [
            'user_id' => $personId,
            'slug' => 'fill'
        ];
        $fill = $this->userSettingsRepository->whereLast($where);
        if(!empty($theme)) $settings['theme'] = $theme->value;
        if(!empty($fill)) $settings['fill'] = $fill->value;

        return $settings;
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function headerMenus($personId)
    {
        $where = ['user_id' => $personId];
        $userHeaderMenuIds = $this->userHeaderMenuRepository->pluckWhere($where, 'header_menu_id');
        return $this->headerMenuRepository->whereIn('id' , $userHeaderMenuIds);
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function profession($personId)
    {
        $where = ['user_id' => $personId];

        return $this->userProfessionRepository->whereLast($where);
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function technicalSkills($personId)
    {
        $where = ['user_id' => $personId];

        return $this->technicalSkillRepository->getWhere($where);
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function professionalSkills($personId)
    {
        $where = ['user_id' => $personId];

        return $this->professionalSkillRepository->getWhere($where);
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function about($personId)
    {
        $where = ['user_id' => $personId];

        return $this->aboutRepository->whereLast($where);
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function activities($personId)
    {
        $where = ['user_id' => $personId];

        return $this->userActivityRepository->getWhere($where);
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function featuredProjects($personId)
    {
        $where = ['user_id' => $personId];
        $projects = $this->userFeaturedProjectRepository->getWhere($where);
        foreach ($projects as $project){
            $where = ['user_featured_project_id' => $project->id];
            $project['comments'] = $this->userFeaturedProjectCommentRepository->getWhere($where);
        }

        return $projects;
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function educations($personId)
    {
        $where = ['user_id' => $personId];
        $educations = $this->educationRepository->getWhere($where);
        foreach ($educations as $education){
            $education['year_started'] = explode('-', $education->started_at)[0];
            $education['year_ended'] = explode('-', $education->ended_at)[0];
        }

        return $educations;
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function workExperiences($personId)
    {
        $where = ['user_id' => $personId];
        $workExperiences = $this->workExperienceRepository->getWhere($where);
        foreach ($workExperiences as $workExperience){
            $workExperience['responsibilities'] = explode(',', $workExperience->responsibility);
            $workExperience['year_started'] = explode('-', $workExperience->started_at)[0];
            $workExperience['year_ended'] = explode('-', $workExperience->ended_at)[0];
        }

        return $workExperiences;
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function portfolioCategories($personId)
    {
        $where = ['user_id' => $personId];
        $portfolioCategories = $this->portfolioCategoryRepository->getWhere($where);
        foreach ($portfolioCategories as $portfolioCategory){
            $portfolioCategory['filter_key'] = preg_replace('/\s+/', '-', strtolower($portfolioCategory->title));
        }

        return $portfolioCategories;
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function portfolioContents($personId)
    {
        $where = ['user_id' => $personId];
        $portfolioCategoryIds = $this->portfolioCategoryRepository->pluckWhere($where, 'id');
        $portfolioContents = $this->portfolioContentRepository->whereIn('portfolio_category_id', $portfolioCategoryIds);
        foreach ($portfolioContents as $portfolioContent) {
            $where = ['id' => $portfolioContent->portfolio_category_id];
            $portfolioCategory = $this->portfolioCategoryRepository->whereLast($where);
            $portfolioContent['class'] = preg_replace('/\s+/', '-', strtolower($portfolioCategory->title));
        }

        return $portfolioContents;
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function prices($personId)
    {
        $where = ['user_id' => $personId];
        $prices = $this->priceRepository->getWhere($where);
        foreach ($prices as $price){
            $price['contents'] = explode(',', $price->content);
        }

        return $prices;
    }

    /**
     * @param $personId
     * @return mixed
     */
    public function posts($personId)
    {
        $where = ['user_id' => $personId];

        return $this->postRepository->getWhere($where);
    }

    /**
     * @param $request
     * @return array
     */
    public function saveMessage($request)
    {
        try{
            $contactMessage = $this->prepareContactMessageData($request);
            $this->contactMessageRepository->create($contactMessage);

            return [
                'success' => true,
                'message' => 'Message Sent',
                'data' => [],
                'webResponse' => [
                    'success' => 'Message Sent'
                ],
            ];
        }catch (\Exception $e){
            return $this->errorResponse;
        }
    }

    /**
     * @param $request
     * @return array
     */
    private function prepareContactMessageData($request)
    {
        return [
            'user_id' => $request->user_id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'message' => $request->message,
        ];
    }
}
