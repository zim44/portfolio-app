<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //Authentication
        $this->mapAuthenticationWebRoutes();

        //User Account Messages Home
        $this->mapUserAccountMessagesWebRoutes();

        //User Customization
        $this->mapUserCustomizationWebRoutes();

        //User Header Menu Home
        $this->mapUserHeaderMenuHomeWebRoutes();

        //User Header Menu About
        $this->mapUserHeaderMenuAboutWebRoutes();

        //User Header Menu Skills
        $this->mapUserHeaderMenuSkillsWebRoutes();

        //User Header Menu Experiences
        $this->mapUserHeaderMenuExperiencesWebRoutes();

        //User Header Menu Portfolio
        $this->mapUserHeaderMenuPortfolioWebRoutes();

        //User Header Menu Pricing
        $this->mapUserHeaderMenuPricingWebRoutes();

        //User Header Menu Blog
        $this->mapUserHeaderMenuBlogWebRoutes();

        //User Header Menu Contact
        $this->mapUserHeaderMenuContactWebRoutes();

        //User Settings
        $this->mapUserSettingsWebRoutes();

        //User Profile
        $this->mapUserProfileWebRoutes();

        //User Preview
        $this->mapUserPreviewWebRoutes();

        //Visitor
        $this->mapVisitorWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(base_path('routes/web.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * Authentication web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapAuthenticationWebRoutes() {
        Route::prefix('/')
            ->middleware(['web'])
            ->namespace( 'App\Modules\Authentication\Controllers\Web')
            ->group(app_path('Modules/Authentication/Routes/Web/auth.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * User Account Messages web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapUserAccountMessagesWebRoutes() {
        Route::prefix('account')
            ->middleware(['web'])
            ->namespace( 'App\Modules\Account\Messages\Controllers\Web\User')
            ->group(app_path('Modules/Account/Messages/Routes/User/Web/messages.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * User Customization web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapUserCustomizationWebRoutes() {
        Route::prefix('/')
            ->middleware(['web'])
            ->namespace( 'App\Modules\Customization\Controllers\Web\User')
            ->group(app_path('Modules/Customization/Routes/User/Web/customization.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * User Header Menu Home web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapUserHeaderMenuHomeWebRoutes() {
        Route::prefix('header-menu')
            ->middleware(['web'])
            ->namespace( 'App\Modules\HeaderMenus\Home\Controllers\Web\User')
            ->group(app_path('Modules/HeaderMenus/Home/Routes/User/Web/home.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * User Header Menu About web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapUserHeaderMenuAboutWebRoutes() {
        Route::prefix('header-menu')
            ->middleware(['web'])
            ->namespace( 'App\Modules\HeaderMenus\About\Controllers\Web\User')
            ->group(app_path('Modules/HeaderMenus/About/Routes/User/Web/about.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * User Header Menu Skills web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapUserHeaderMenuSkillsWebRoutes() {
        Route::prefix('header-menu')
            ->middleware(['web'])
            ->namespace( 'App\Modules\HeaderMenus\Skills\Controllers\Web\User')
            ->group(app_path('Modules/HeaderMenus/Skills/Routes/User/Web/skills.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * User Header Menu Experiences web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapUserHeaderMenuExperiencesWebRoutes() {
        Route::prefix('header-menu')
            ->middleware(['web'])
            ->namespace( 'App\Modules\HeaderMenus\Experiences\Controllers\Web\User')
            ->group(app_path('Modules/HeaderMenus/Experiences/Routes/User/Web/experiences.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * User Header Menu Portfolio web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapUserHeaderMenuPortfolioWebRoutes() {
        Route::prefix('header-menu')
            ->middleware(['web'])
            ->namespace( 'App\Modules\HeaderMenus\Portfolio\Controllers\Web\User')
            ->group(app_path('Modules/HeaderMenus/Portfolio/Routes/User/Web/portfolio.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * User Header Menu Pricing web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapUserHeaderMenuPricingWebRoutes() {
        Route::prefix('header-menu')
            ->middleware(['web'])
            ->namespace( 'App\Modules\HeaderMenus\Pricing\Controllers\Web\User')
            ->group(app_path('Modules/HeaderMenus/Pricing/Routes/User/Web/pricing.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * User Header Menu Blog web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapUserHeaderMenuBlogWebRoutes() {
        Route::prefix('header-menu')
            ->middleware(['web'])
            ->namespace( 'App\Modules\HeaderMenus\Blog\Controllers\Web\User')
            ->group(app_path('Modules/HeaderMenus/Blog/Routes/User/Web/blog.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * User Header Menu Contact web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapUserHeaderMenuContactWebRoutes() {
        Route::prefix('header-menu')
            ->middleware(['web'])
            ->namespace( 'App\Modules\HeaderMenus\Contact\Controllers\Web\User')
            ->group(app_path('Modules/HeaderMenus/Contact/Routes/User/Web/contact.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * User Settings web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapUserSettingsWebRoutes() {
        Route::prefix('/')
            ->middleware(['web'])
            ->namespace( 'App\Modules\Settings\Controllers\Web\User')
            ->group(app_path('Modules/Settings/Routes/User/Web/settings.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * User Profile web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapUserProfileWebRoutes() {
        Route::prefix('/')
            ->middleware(['web'])
            ->namespace( 'App\Modules\Profile\Controllers\Web\User')
            ->group(app_path('Modules/Profile/Routes/User/Web/profile.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * User Preview web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapUserPreviewWebRoutes() {
        Route::prefix('/')
            ->middleware(['web'])
            ->namespace( 'App\Modules\Preview\Controllers\Web\User')
            ->group(app_path('Modules/Preview/Routes/User/Web/preview.php'));
    }

    /**
     * ---------------------------------------------------------------------------------------
     * Visitor web route for admin
     * ---------------------------------------------------------------------------------------
     */
    protected function mapVisitorWebRoutes() {
        Route::prefix('/')
            ->middleware(['web'])
            ->namespace( 'App\Modules\Visitor\Controllers\Web')
            ->group(app_path('Modules/Visitor/Routes/Web/visitor.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->namespace)
            ->group(base_path('routes/api.php'));
    }
}
